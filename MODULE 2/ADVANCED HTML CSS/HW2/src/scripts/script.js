const menuBttn = document.querySelector(".header__bttn");
const headerNav = document.querySelector(".header__nav");
const burgerWrapper = document.querySelector(".header__burger-wrapper");
const xWrapper = document.querySelector(".header_x-wrapper");
const headerList = document.querySelector(".header__list");
const headerItem = document.querySelector(".header__item");

// MENU BUTTON EVENT LISTENER
menuBttn.addEventListener("click", (event) => {
  headerNav.classList.toggle("header__nav--closed");
  burgerWrapper.classList.toggle("header__burger-wrapper--off");
  xWrapper.classList.toggle("header_x-wrapper--off");
});

// MENU LIST EVENT LISTENER
headerList.addEventListener("click", (event) => {
  if (event.currentTarget !== event.target) {
    document
      .querySelector(".header__item--selected")
      .classList.remove("header__item--selected");
    event.target.classList.add("header__item--selected");
  }
});
