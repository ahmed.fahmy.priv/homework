import gulp from "gulp";
import clean from "gulp-clean";
import imagemin from "gulp-imagemin";
import concat from "gulp-concat";
import terser from "gulp-terser";
import autoprefixer from "gulp-autoprefixer";
import cssMin from "gulp-clean-css";
import dartSass from "sass";
import gulpSass from "gulp-sass";
const sass = gulpSass(dartSass);
import browserSync from "browser-sync";
const bS = browserSync.create();

export const cleanDist = () =>
  gulp.src("./dist", { read: false, allowEmpty: true }).pipe(clean());

const imgMin = () =>
  gulp.src("src/images/**/*").pipe(imagemin()).pipe(gulp.dest("dist/images"));

const sassCompile = () =>
  gulp.src("./src/**/*.scss").pipe(sass()).pipe(gulp.dest("./src/css"));

const css = () =>
  gulp
    .src("./src/**/*.css")
    .pipe(
      autoprefixer({
        cascade: false,
      })
    )
    .pipe(concat("styles.min.css"))
    .pipe(cssMin())
    .pipe(gulp.dest("./dist"));

const js = () =>
  gulp
    .src("./src/**/*.js")
    .pipe(concat("scripts.min.js"))
    .pipe(terser())
    .pipe(gulp.dest("./dist/"));

export const build = gulp.series(imgMin, sassCompile, gulp.parallel(css, js));

export const dev =
  (build,
  () => {
    bS.init({
      server: {
        baseDir: "./",
      },
    });

    gulp.watch(
      "./src/sass/**",
      gulp.series(sassCompile, (done) => {
        bS.reload();
        done();
      })
    );

    gulp.watch(
      "./src/css/**",
      gulp.series(css, (done) => {
        bS.reload();
        done();
      })
    );

    gulp.watch(
      "./src/scripts/**",
      gulp.series(js, (done) => {
        bS.reload();
        done();
      })
    );

    gulp.watch(
      "./index.html",
      gulp.series((done) => {
        bS.reload();
        done();
      })
    );

    gulp.watch(
      "./src/images/**",
      gulp.series(imgMin, (done) => {
        bS.reload();
        done();
      })
    );
  });
