// Задание
// Реализовать функцию, которая будет производить математические операции с введеными пользователем числами. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// * Технические требования:

// - Считать с помощью модального окна браузера два числа.
// - Считать с помощью модального окна браузера математическую операцию, которую нужно совершить. Сюда может быть введено +, -, *, /.
// - Создать функцию, в которую передать два значения и операцию.
// - Вывести в консоль результат выполнения функции.

// * Необязательное задание продвинутой сложности:

// - После ввода данных добавить проверку их корректности. Если пользователь не ввел числа, либо при вводе указал не числа, - спросить оба числа заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).

// function getOperator() {
//   let mathOperator;

//   do {
//     mathOperator = prompt("Please choose a mathematical operator: + - / *");
//   } while (
//     mathOperator !== "+" &&
//     mathOperator !== "-" &&
//     mathOperator !== "*" &&
//     mathOperator !== "/"
//   );
//   return mathOperator;
// }

// function getNumber(message) {
//   let userNumber;

//   do {
//     userNumber = prompt(message);
//   } while (isNaN(userNumber) || userNumber === "" || userNumber == null);
//   return +userNumber;
// }

// function calcSum() {
//   const firstNum = getNumber("Please enter the first number");
//   const secondNum = getNumber("Please enter the second number");
//   return firstNum + secondNum;
// }

// function calcSub() {
//   const firstNum = getNumber("Please enter the first number");
//   const secondNum = getNumber("Please enter the second number");
//   return firstNum - secondNum;
// }
// function calcMulti() {
//   const firstNum = getNumber("Please enter the first number");
//   const secondNum = getNumber("Please enter the second number");
//   return firstNum * secondNum;
// }
// function calcDiv() {
//   const firstNum = getNumber("Please enter the first number");
//   const secondNum = getNumber("Please enter the second number");
//   return firstNum / secondNum;
// }

// const calcResult = () => {
//   const finalResult = getOperator();

//   switch (finalResult) {
//     case "+":
//       calcSum();
//       break;
//     case "-":
//       console.log(calcSub());
//       break;
//     case "*":
//       console.log(calcMulti());
//       break;
//     case "/":
//       console.log(calcDiv());
//       break;
//   }
// };

// console.log(calcResult());

// HW4 REWRITTEN AND EDITED:

function getOperator() {
  let mathOperator;

  do {
    mathOperator = prompt(
      "Please choose a mathematical operator:",
      "+, -, /, *"
    );
  } while (
    mathOperator !== "+" &&
    mathOperator !== "-" &&
    mathOperator !== "*" &&
    mathOperator !== "/"
  );
  return mathOperator;
}

function getNumber(message) {
  let userNumber;

  do {
    userNumber = prompt(message);
  } while (isNaN(userNumber) || userNumber === "" || userNumber == null);
  return +userNumber;
}

const calcResult = (operator, firstNum, secondNum) => {
  switch (operator) {
    case "+":
      return firstNum + secondNum;

    case "-":
      return firstNum - secondNum;

    case "*":
      return firstNum * secondNum;

    case "/":
      return firstNum / secondNum;
  }
};

console.log(
  calcResult(
    getOperator(),
    getNumber("Please enter the first number"),
    getNumber("Please enter the second number")
  )
);
