// Необязательное задание продвинутой сложности:

// Проверить, что введенное значение является целым числом. Если данное условие не соблюдается, повторять вывод окна на экран до тех пор, пока не будет введено целое число.    DONE
// Считать два числа, m и n. Вывести в консоль все простые числа (http://ru.math.wikia.com/wiki/%D0%9F%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B5_%D1%87%D0%B8%D1%81%D0%BB%D0%BE) в диапазоне от m до n (меньшее из введенных чисел будет m, бОльшее будет n). Если хотя бы одно из чисел не соблюдает условие валидации, указанное выше, вывести сообщение об ошибке, и спросить оба числа заново.

let m;
let n;

do {
  m = +prompt("Please enter your lower threshold number starting from 2", "2");
  n = +prompt("Please enter your upper threshold number");
} while (
  !Number.isInteger(m) ||
  !Number.isInteger(n) ||
  m <= 0 ||
  n <= 0 ||
  isNaN(m) ||
  isNaN(n) ||
  m >= n ||
  m == 1
);

nextPrime: for (let i = m; i <= n; i++) {
  for (let j = 2; j < i; j++) {
    if (i % j == 0) continue nextPrime;
  }

  console.log(i);
}
