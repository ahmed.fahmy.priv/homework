// Задание
// Реализовать программу на Javascript, которая будет находить все числа кратные 5 (делятся на 5 без остатка) в заданном диапазоне. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// Технические требования:

// Считать с помощью модального окна браузера число, которое введет пользователь.        DONE
// Вывести в консоли все числа кратные 5, от 0 до введенного пользователем числа. Если таких чисел нету - вывести в консоль фразу `Sorry, no numbers'   DONE
// Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.   DONE
// Проверить, что введенное значение является целым числом. Если данное условие не соблюдается, повторять вывод окна на экран до тех пор, пока не будет введено целое число.    DONE

let userNum;

do {
  userNum = +prompt("Please enter a number");
} while (!Number.isInteger(userNum) || userNum <= 0);

if (userNum < 5) {
  console.log("Sorry, no numbers");
}

for (i = 5; i <= userNum; i++) {
  if (i % 5 == 0) {
    console.log(i);
  }
}
