// Задание

// Реализовать простую программу на Javascript, которая будет взаимодействовать с пользователем с помощью модальных окон браузера - alert, prompt, confirm.
// Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// Технические требования:

// 1- Считать с помощью модельного окна браузера данные пользователя: имя и возраст.     DONE

// 2- Если возраст меньше 18 лет - показать на экране сообщение: You are not allowed to visit this website.  DONE

// 3- Если возраст от 18 до 22 лет (включительно) - показать окно со следующим сообщением: Are you sure you want to continue? и кнопками Ok, Cancel.
// Если пользователь нажал Ok, показать на экране сообщение: Welcome,  + имя пользователя. Если пользователь нажал Cancel,
// показать на экране сообщение: You are not allowed to visit this website.                DONE

// 4- Если возраст больше 22 лет - показать на экране сообщение: Welcome,  + имя пользователя.   DONE

// 5- Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.  DONE

// 6- После ввода данных добавить проверку их корректности. Если пользователь не ввел имя, либо при вводе возраста указал не число - спросить имя и возраст заново
// (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).

let userName = prompt("Please enter your name :", "Mike");

while (userName === "" || userName === null) {
  userName = prompt("Please enter your name :", "Mike");
}

let age = +prompt("Please enter your age :", "18");

while (isNaN(age) || age === 0) {
  age = +prompt("Please enter your age :", "18");
}

if (age < 18) {
  alert("You are not allowed to visit this website.");
} else if (age >= 18 && age <= 22) {
  age = confirm("Are you sure you want to continue?");

  if (age) {
    alert("Welcome, " + userName);
  } else alert("You are not allowed to visit this website.");
} else alert("Welcome " + userName);
