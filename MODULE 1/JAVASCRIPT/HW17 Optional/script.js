// Технические требования:

// * Создать пустой объект student, с полями name и lastName.   DONE
// * Спросить у пользователя имя и фамилию студента, полученные значения записать в соответствующие поля объекта.
// * В цикле спрашивать у пользователя название предмета и оценку по нему. Если пользователь нажмет Cancel при n-вопросе о названии предмета, закончить цикл. Записать оценки по всем предметам в свойство студента tabel.
// * Посчитать количество плохих (меньше 4) оценок по предметам. Если таких нет, вывести сообщение Студент переведен на следующий курс.
// * Посчитать средний балл по предметам. Если он больше 7 - вывести сообщение Студенту назначена стипендия.

const student = {
  name: undefined,
  lastName: undefined,
  table: {},
};

/**
 * [the function fills the student Object with Name and Lastname and a nested object consisting of a key of the subject name and value of the score.]
 */

const getUserInfoAndSubjectScore = () => {
  student.name = prompt("What is your name?");
  student.lastName = prompt("What is your last name?");

  let subject;

  while (true) {
    subject = prompt("What is the subject?");
    if (subject) {
      student.table[subject] = +prompt("What is the score?");
    } else break;
  }
};

const scoreArr = [];
let failedSubjects = 0;

/**
 * [the function checks the subject scores and acts accordingly]
 */

const checkAndCalculateScore = () => {
  for (key in student.table) {
    if (student.table[key] < 4) {
      failedSubjects += 1;
    }
    scoreArr.push(student.table[key]);
  }

  if (failedSubjects == 0) {
    alert("Студент переведен на следующий курс.");
  }

  let scoreSum = 0;

  scoreArr.forEach((score) => {
    scoreSum += score;
  });

  if (scoreSum / Object.keys(student.table).length > 7) {
    alert("Студенту назначена стипендия.");
  }
};

getUserInfoAndSubjectScore();
checkAndCalculateScore();

console.log(student);
