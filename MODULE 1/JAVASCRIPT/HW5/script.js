// Технические требования:

// * Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.   DONE
// * При вызове функция должна спросить у вызывающего имя и фамилию.                          DONE
// * Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.  DONE
// * Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).                    DONE
// * Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). Вывести в консоль результат выполнения функции. DONE

// Необязательное задание продвинутой сложности:

// * Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить данные свойства.              DONE

"use strict";

function createNewUser() {
  return {
    firstName: null,
    lastName: null,

    getLogin: function () {
      return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;
    },
    setFirstName: function (newValue) {
      Object.defineProperties(newUser, {
        firstName: {
          value: newValue,
        },
      });
    },
    setLastName: function (newValue) {
      Object.defineProperties(newUser, {
        lastName: {
          value: newValue,
        },
      });
    },
  };
}

let newUser = createNewUser(); // we call the function and create the object newUser in this line

// we create the values of the object's properties by receiving them from the user and we protect the properties from being edited.
Object.defineProperties(newUser, {
  firstName: {
    writable: false,
    configurable: true,
    value: prompt("Please enter your first name"),
  },
  lastName: {
    writable: false,
    configurable: true,
    value: prompt("Please enter your last name"),
  },
});

// we test the succesfully created object by the below code and we call the getLogin function.
console.log(newUser);
console.log(newUser.getLogin());

// we test the setter function created succesfully in the below code and its capability of chaging the values of both object properties.
newUser.setFirstName("TEST1");
newUser.setLastName("TEST2");
console.log(newUser);

// we test changing the property values directly but the test fails due to us protecting the property edit by disabling the writable descriptor
newUser.firstName = "TEST1";
newUser.lastName = "TEST2";
console.log(newUser);
