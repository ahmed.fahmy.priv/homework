// Задание
// Добавить в домашнее задание HTML/CSS Flex/Grid различные эффекты с использованием jQuery

// Технические требования:

// 1- Добавить вверху страницы горизонтальное меню со ссылкой на все разделы страницы.       DONE
// 2- При клике на ссылку - плавно прокручивать страницу вниз до выбранного места.     DONE
// 3- Если прокрутить страницу больше чем на 1 экран вниз, справа внизу должна появляться кнопка "Наверх" с фиксированным позиционариванием. При клике на нее - плавно прокрутить страницу в самый верх.      DONE
// 4- Добавить под одной из секций кнопку, которая будет выполнять функцию slideToggle() (прятать и показывать по клику) для данной секции.  DONE

// Технические требования 2 :
// I REALIZED THIS REQUEST BY THE BELOW METHOD OF ADDING ANCHOR LINKS.
// ANORHER IDEA OF REALIZING WAS TO USE SCROLL TO ITEM TOPOFFSET WHEN CLICKING ON LINK.

$(".menu-item:first-child a").attr("href", "#mostPopularPosts");
$(".menu-item:nth-child(2) a").attr("href", "#mostPopularClients");
$(".menu-item:nth-child(3) a").attr("href", "#topRated");
$(".menu-item:last-child a").attr("href", "#hotNews");

$(".popular-posts").prepend('<a name="mostPopularPosts"></a>');
$(".section-popular-clients").prepend('<a name="mostPopularClients"></a>');
$(".top-rated").prepend('<a name="topRated"></a>');
$(".show-hide-hotnews").prepend('<a name="hotNews"></a>');

// Технические требования 3 :

const scrollToTopButton = document.createElement("button");

$(scrollToTopButton).addClass("top-scroll");
$(scrollToTopButton).text("Top");

$(".page__header").append(scrollToTopButton);

$(window).scroll(function () {
  if (window.pageYOffset > document.documentElement.clientHeight) {
    $(scrollToTopButton).show();
  } else {
    $(scrollToTopButton).hide();
  }
});

$(scrollToTopButton).click(function () {
  window.scrollTo({
    top: 0,
    behavior: "smooth",
  });
});

// Технические требования 4:

const slideToggle = () => {
  $(".hot-news").toggle();
};

$(".show-hide-hotnews").click(slideToggle);
