// Задание
// Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// Технические требования:

// * Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:

// 1- При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.    DONE
// 2- Создать метод getAge() который будет возвращать сколько пользователю лет.         DONE
// 3- Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).                     DONE

// * Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.

"use strict";

function createNewUser() {
  return {
    firstName: prompt("Please enter your first name"),
    lastName: prompt("Please enter your last name"),
    birthdate: prompt("Please enter your birth date", "dd.mm.yyyy"),

    getLogin: function () {
      return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;
    },

    getAge: function () {
      const currentDate = new Date();
      const userAge = currentDate.getFullYear() - +this.birthdate.slice(6);
      if (
        (+this.birthdate.slice(3, 5) == currentDate.getMonth() + 1 &&
          +this.birthdate.slice(0, 2) > currentDate.getDate()) ||
        +this.birthdate.slice(3, 5) > currentDate.getMonth() + 1
      ) {
        return `Age: ${userAge - 1} years`;
      } else return `Age: ${userAge} years`;
    },

    getPassword: function () {
      return `Password: ${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${this.birthdate.slice(
        6
      )}`;
    },

    setFirstName: function (newValue) {
      Object.defineProperties(newUser, {
        firstName: {
          value: newValue,
        },
      });
    },

    setLastName: function (newValue) {
      Object.defineProperties(newUser, {
        lastName: {
          value: newValue,
        },
      });
    },
  };
}

let newUser = createNewUser();

Object.defineProperties(newUser, {
  firstName: {
    writable: false,
    configurable: true,
  },
  lastName: {
    writable: false,
    configurable: true,
  },
});

console.log(newUser);

console.log(newUser.getAge());

console.log(newUser.getPassword());
