// Задание
// Реализовать переключение вкладок (табы) на чистом Javascript.

// Технические требования:

// * В папке tabs лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться для какой вкладки.

// * Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.

// * Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться. При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.

const tabsCollection = document.querySelector(".tabs");
const tabsContentCollection = document.querySelectorAll(".tabs-content li");

//FIRST SOLUTION WITHOUT DELEGATION

// const switchContent = () => {
//   tabsCollection.forEach((tabItem) => {
//     tabItem.addEventListener("click", (event) => {
//       document.querySelector(".active").classList.remove("active");
//       event.target.classList.add("active");
//       document.querySelector(".active-text").classList.remove("active-text");
//       tabsContentCollection.forEach((contentItem) => {
//         if (contentItem.dataset.title == event.target.dataset.title) {
//           contentItem.classList.add("active-text");
//         }
//       });
//     });
//   });
// };

// switchContent();

//SECOND SOLUTION WITH DELEGATION

const switchContent = () => {
  tabsCollection.addEventListener("click", (event) => {
    document.querySelector(".active").classList.remove("active");
    event.target.classList.add("active");

    document.querySelector(".active-text").classList.remove("active-text");
    tabsContentCollection.forEach((contentItem) => {
      if (contentItem.dataset.title == event.target.dataset.title) {
        contentItem.classList.add("active-text");
      }
    });
  });
};

switchContent();
