// ## Задание

// Реализовать функцию подсветки нажимаемых клавиш. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// #### Технические требования:
// - В файле `index.html` лежит разметка для кнопок.        DONE

// - Каждая кнопка содержит в себе название клавиши на клавиатуре   DONE

// - По нажатию указанных клавиш - та кнопка, на которой написана эта буква, должна окрашиваться в синий цвет. При этом, если какая-то другая буква уже ранее была окрашена в синий цвет - она становится черной. Например по нажатию `Enter` первая кнопка окрашивается в синий цвет. Далее, пользователь нажимает `S`, и кнопка `S` окрашивается в синий цвет, а кнопка `Enter` опять становится черной.   DONE

const buttonCollection = document.querySelectorAll(".btn");

window.addEventListener("keydown", (event) => {
  buttonCollection.forEach((button) => {
    button.style.backgroundColor = "black";
    if (
      event.key.toUpperCase() == button.innerText ||
      event.key == button.innerText
    ) {
      button.style.backgroundColor = "blue";
    }
  });
});
