// #### Технические требования:
// - В файле `index.html` лежит разметка для двух полей ввода пароля.   DONE

// - По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, иконка меняет свой внешний вид. В комментариях под иконкой - иконка другая, именно она должна отображаться вместо текущей.    DONE

// - Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)        DONE

// - Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)   DONE

// - По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях   DONE

// - Если значения совпадают - вывести модальное окно (можно alert) с текстом - `You are welcome`;         DONE

// - Если значение не совпадают - вывести под вторым полем текст красного цвета  `Нужно ввести
// одинаковые значения`         DONE

// - После нажатия на кнопку страница не должна перезагружаться   DONE

// - Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.  DONE

const passwordForm = document.querySelector("form");
const password = passwordForm.querySelector(".password");
const confirmPassword = passwordForm.querySelector(".confirm-password");
const error = passwordForm.querySelector("span");

// SOLUTION WITHOUT DELEGATION

// const iconCollection = document.querySelectorAll("i");

// iconCollection.forEach((icon) => {
//   icon.addEventListener("click", (event) => {
//     event.target.classList.toggle("fa-eye-slash");
//     if (event.target.classList.contains("fa-eye-slash")) {
//       event.target.previousElementSibling.type = "text";
//     } else event.target.previousElementSibling.type = "password";
//   });
// });

// SOLUTION USING DELEGATION

passwordForm.addEventListener("click", (event) => {
  if (event.target.tagName == "I") {
    event.target.classList.toggle("fa-eye-slash");
  }
  if (event.target.classList.contains("fa-eye-slash")) {
    event.target.previousElementSibling.type = "text";
  } else event.target.previousElementSibling.type = "password";
});

passwordForm.addEventListener("submit", (event) => {
  // После нажатия на кнопку страница не должна перезагружаться
  event.preventDefault();
  passwordForm.querySelector("span").innerText = "";
  if (
    !password.value ||
    !confirmPassword.value ||
    password.value !== confirmPassword.value
  ) {
    error.innerText = "Нужно ввести одинаковые значения";
  } else {
    alert("You are welcome");
  }
});
