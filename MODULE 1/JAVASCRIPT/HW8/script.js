// Задания
// Код для заданий лежит в папке project.

// 1- Найти все параграфы на странице и установить цвет фона #ff0000     DONE

// 2- Найти элемент с id="optionsList". Вывести в консоль. Найти родительский элемент и вывести в консоль. Найти дочерние ноды, если они есть, и вывести в консоль названия и тип нод.       DONE

// 3- Установите в качестве контента элемента с классом testParagraph следующий параграф
// This is a paragraph    DONE

// 4- Получить элементы вложенные в элемент с классом main-header и вывеcти их в консоль. Каждому из элементов присвоить новый класс nav-item.   DONE

// 5- Найти все элементы с классом section-title. Удалить этот класс у элементов.      DONE

// 1-
const parags = document.querySelectorAll("p");

// console.log(parags);

parags.forEach((p) => (p.style.backgroundColor = "#ff0000"));

// 2-

console.log(document.getElementById("optionsList"));

console.log(optionsList.parentElement);

console.log(optionsList?.childNodes);

// 3-
const testP = document.getElementById("testParagraph");

testP.innerText = "This is a paragraph";
// console.log(testP);

// // 4-
const mainHeaderElements = document.querySelector(".main-header").children;

console.log(mainHeaderElements);

for (elem of mainHeaderElements) {
  elem.classList.add("nav-item");
  // console.log(elem);
}

// 5-  THE BELOW CODE IS WRITTEN USING A FOR OF CYCLE ASSUMING THAT WE HAVE MORE THAN 1 ELEMENT WITH THE SECTION-TITLE CLASS
const sectionTitle = document.querySelectorAll(".section-title");
// console.log(sectionTitle);

sectionTitle.forEach((elem) => elem.classList.remove("section-title"));
// console.log(sectionTitle);

//IF WE WANTED TO ONILY SEARCH FOR ONE ELEMENT WE COULD HAVE USED THE CODE BELOW:

// const sectionTitle = document.querySelector(".section-title");
// sectionTitle.classList.remove("section-title");
// console.log(sectionTitle);
