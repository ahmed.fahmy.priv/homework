1- Опишите своими словами разницу между функциями setTimeout() и setInterval().

- setTimeout() : ждет столько времени сколько мы укажем в аргументе и потом выполнить код который написан в ней 1 раз

- setInterval() : ждет столько времени сколько мы укажем в аргументе и потом выполнить код который написан в ней повторно без конечности раз спустя столько времени как мы указали

2- Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?

- нет не сработает мгновенно а наоборот только сработает после выполнения весь скрипт
- почему? потому что в таком случае код идет в Виндов АПИ а потом в QUEUE и ждет пока STACK JAVASCRIPT выполняется до конца

3- Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?

- чтобы функция не сработала без конечности и загрузила браузер память и процессор
