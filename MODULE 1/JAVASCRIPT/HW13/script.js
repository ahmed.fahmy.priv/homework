// Задание
// Реализовать программу, показывающую циклично разные картинки. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// Технические требования:

// * В папке banners лежит HTML код и папка с картинками.          DONE
// * При запуске программы на экране должна отображаться первая картинка.  DONE
// * Через 3 секунды вместо нее должна быть показана вторая картинка.    DONE
// * Еще через 3 секунды - третья.             DONE
// * Еще через 3 секунды - четвертая.             DONE
// * После того, как покажутся все картинки - этот цикл должен начаться заново. DONE
// * При запуске программы где-то на экране должна появиться кнопка с надписью Прекратить.    DONE
// * По нажатию на кнопку Прекратить цикл завершается, на экране остается показанной та картинка, которая была там при нажатии кнопки. DONE
// * Рядом с кнопкой Прекратить должна быть кнопка Возобновить показ, при нажатии которой цикл продолжается с той картинки, которая в данный момент показана на экране.   DONE
// * Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.   DONE

// Необязательное задание продвинутой сложности:

// * При запуске программы на экране должен быть таймер с секундами и миллисекундами, показывающий сколько осталось до показа следующей картинки.
// * Делать скрытие картинки и показывание новой картинки постепенным (анимация fadeOut / fadeIn) в течение 0.5 секунды.    DONE

const imageCollection = document.querySelectorAll(".image-to-show");
const buttonContainer = document.querySelector(".button-wrapper");
const timerText = document.querySelector(".timer");

// COUNT DOWN TIMER FUNCTION

let now;
let timeToCountDown;
let timeDifferenceMs;
let seconds;
let intervalId;

/**
 *
 * @param {time in milliseconds} timeTocountDown
 * @description a function that creates and shows a countdown timer
 */

const showCountDownTimer = () => {
  clearInterval(intervalId);
  timeToCountDown = new Date().getTime() + 3000;

  intervalId = setInterval(() => {
    now = new Date().getTime();
    timeDifferenceMs = timeToCountDown - now;
    seconds = parseInt(timeDifferenceMs / 1000);
    // debugger;
    if (timeDifferenceMs >= -1) {
      timerText.innerHTML = `${seconds}s ${timeDifferenceMs} ms`;
    }
    // } else clearInterval(intervalId);
  }, 1);
};

// IMAGE CAROUSEL FUNCTION

let index = 0;
let timerId;

function changeImage() {
  imageCollection.forEach((img) => {
    img.style.display = "none";
  });

  showCountDownTimer();

  imageCollection[index].style.display = "block";
  index++;
  if (index >= imageCollection.length) {
    index = 0;
  }
  timerId = setTimeout(changeImage, 3000);
}

changeImage();
// BUTTON FUNCTIONALITY EVENT LISTENERS

buttonContainer.addEventListener("click", (event) => {
  // START BUTTON FUNCTIONALITY
  // if (event.target.dataset.function == "start") {
  //   clearInterval(intervalId);
  //   clearTimeout(timerId);
  //   index = 0;
  //   changeImage();
  //   event.currentTarget.children[2].style.display = "none";
  //   event.target.nextElementSibling.style.display = "inline";

  // STOP BUTTON FUNCTIONALITY
  if (event.target.dataset.function == "stop") {
    clearInterval(intervalId);
    clearTimeout(timerId);
    timerText.innerHTML = "Timer Stopped";
    // event.target.nextElementSibling.style.display = "inline";

    // RESUME BUTTON FUNCTIONALITY
  } else if (event.target.dataset.function == "resume") {
    clearInterval(intervalId);
    clearTimeout(timerId);
    showCountDownTimer();
    index = index;
    timerId = setTimeout(changeImage, 3000);
    // event.target.style.display = "none";
  }
});
