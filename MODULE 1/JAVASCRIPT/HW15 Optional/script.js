let number;

const getAndCheckNumber = () => {
  do {
    number = prompt(
      "Please enter a number",
      "Any number starting from 0 and more"
    );
  } while (isNaN(number) || number == undefined || number < 0);
  return number;
};

let result;

const calcFactorial = (n, m = n - 1) => {
  result = n * m;
  if (m == 0) {
    return alert(`The Factorial of ${number} is: ${n}`);
  } else if (n == 0) {
    return alert(`The Factorial of ${number} is: 1`);
  } else if (m > 1) {
    calcFactorial(result, m - 1);
  } else return alert(`The Factorial of ${number} is: ${result}`);
};

calcFactorial(getAndCheckNumber());
