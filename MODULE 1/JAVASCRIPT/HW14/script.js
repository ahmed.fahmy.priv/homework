// Задание
// Реализовать возможность смены цветовой темы сайта пользователем. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// Технические требования:

// * Взять любое готовое домашнее задание по HTML/CSS.   DONE
// * Добавить на макете кнопку "Сменить тему".            DONE
// * При нажатии на кнопку - менять цветовую гамму сайта (цвета кнопок, фона и т.д.) на ваше усмотрение. При повтором нажатии - возвращать все как было изначально - как будто для страницы доступны две цветовых темы.                    DONE
// * Выбранная тема должна сохраняться и после перезагрузки страницы   DONE

const themeButton = document.querySelector(".theme");
const pageBody = document.querySelector("body");
const logoLink = document.querySelector(".logo-link");

themeButton.addEventListener("click", (event) => {
  pageBody.classList.toggle("body-night-theme");
  logoLink.classList.toggle("logo-night-theme");

  localStorage.setItem("pageBackground", pageBody.className);
  localStorage.setItem("linkColor", logoLink.className);
});

const pageBackground = localStorage.getItem("pageBackground");
const linkColor = localStorage.getItem("linkColor");

if (pageBackground) {
  pageBody.className = pageBackground;
  logoLink.className = linkColor;
}
