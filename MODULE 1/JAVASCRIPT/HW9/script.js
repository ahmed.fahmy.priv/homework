// Задание
// Реализовать функцию, которая будет получать массив элементов и выводить их на страницу в виде списка. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонних библиотек (типа Jquery).

// --- Технические требования:

// * Создать функцию, которая будет принимать на вход массив и опциональный второй аргумент parent - DOM-элемент, к которому будет прикреплен список (по дефолту должен быть document.body).      DONE

// * Каждый из элементов массива вывести на страницу в виде пункта списка;         DONE

// --- Необязательные задания продвинутой сложности:

// 1. Добавьте обработку вложенных массивов. Если внутри массива одним из элементов будет еще один массив, выводить его как вложенный список.
// Пример такого массива:

// Подсказка: используйте map для обхода массива и рекурсию, чтоб обработать вложенные массивы.

// 2. Очистить страницу через 3 секунды. Показывать таймер обратного отсчета (только секунды) перед очищением страницы.

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// FIRST SOLUTION WITHOUT RECURSSION ASSUMING THAT THERE IS NO INTERNAL ARRAYS INSIDE AN ARRAY:

const testArray = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

/**
 *
 * @param {Array} arr
 * @param {Tag, class, id} parent
 * @description Inserts the arr elements in the HTML page as list points/items.
 */

const createList = (arr, parent = document.body) => {
  const htmlArr = arr.map((item) => `<li>${item}</li>`);

  // parent.insertAdjacentHTML("beforeend", htmlArr.join(""));
  parent.innerHTML = htmlArr.join("");
};

// CLEARING THE SCREEN AFTER 3 SECONDS FROM ADDING THE ARRAY LIST ITEMS BUT I WASNT ABLE TO REALIZE THE TIMER COUNTDOWN OPTIONAL TASK.

const clearScreen = () => {
  const clearElem = () => (document.body.innerHTML = "");
  setTimeout(clearElem, 3000);
};

const startBttn = document.querySelector("#bttn");
startBttn.onclick = () => createList(testArray, document.querySelector("ul"));
startBttn.addEventListener("click", clearScreen);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// SECOND SOLUTION WITH RECURSSION ASSUMING THAT THERE ARE INTERNAL ARRAYS INSIDE AN ARRAY:

// const testArray2 = [
//   "Kharkiv",
//   "Kiev",
//   ["Borispol", "Irpin"],
//   "Odessa",
//   "Lviv",
//   "Dnieper",
// ];

// const createList = (arr, parent = document.body) => {
//   const htmlArr = arr.map((item) => {
//     //Base condition to stop recurrsion if not true
//     if (Array.isArray(item)) {
//       const innerUl = document.createElement("ul");
//       parent.append(innerUl);
//       return createList(item, innerUl);
//     } else {
//       return `<li>${item}</li>`;
//     }
//   });

//   parent.insertAdjacentHTML("afterbegin", htmlArr.join(""));
// };

// const clearScreen = () => {
//   const clearElem = () => (document.body.innerHTML = "");
//   setTimeout(clearElem, 3000);
// };

// const startBttn = document.querySelector("#bttn");
// startBttn.onclick = () => createList(testArray2, document.querySelector("ul"));
// startBttn.addEventListener("click", clearScreen);
