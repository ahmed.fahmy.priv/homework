import React from "react";
import styles from "./ModalBg.module.scss";

class ModalBg extends React.Component {
  render() {
    const { onClick } = this.props;

    return <div className={styles.modalBg} onClick={onClick}></div>;
  }
}

export default ModalBg;
