import React from "react";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import styles from "./App.module.scss";

class App extends React.Component {
  state = {
    isFirstModalOpened: false,
    isSecondModalOpened: false,
  };

  firstBtnClickHandler = () => {
    this.setState({ isFirstModalOpened: true });
  };

  secondBtnClickHandler = () => {
    this.setState({ isSecondModalOpened: true });
  };

  firstModalBtnsHandler = () => {
    this.setState({ isFirstModalOpened: false });
  };

  secondModalBtnsHandler = () => {
    this.setState({ isSecondModalOpened: false });
  };

  render() {
    const { isFirstModalOpened, isSecondModalOpened } = this.state;
    return (
      <>
        <div className={styles.mainBtnWrapper}>
          <Button text="Open first modal" backgroundColor="#e74c3c" onClick={this.firstBtnClickHandler} />
          <Button text="Open second modal" backgroundColor="#009E4B" onClick={this.secondBtnClickHandler} />
        </div>

        {isFirstModalOpened && (
          <Modal
            header="Do You want to delete this file?"
            text={
              "Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?"
            }
            actions={
              <>
                <Button text="Ok" backgroundColor="#b3382c" onClick={this.firstModalBtnsHandler} />
                <Button text="Cancel" backgroundColor="#b3382c" onClick={this.firstModalBtnsHandler} />
              </>
            }
            closeButton={true}
            closeModal={this.firstModalBtnsHandler}
          />
        )}

        {isSecondModalOpened && (
          <Modal
            header="Are you sure you want to edit this post?"
            text={"Post title to be edited must be displayed here."}
            actions={
              <>
                <Button text="Yes" backgroundColor="#006B32" onClick={this.secondModalBtnsHandler} />
                <Button text="No" backgroundColor="#006B32" onClick={this.secondModalBtnsHandler} />
              </>
            }
            closeButton={true}
            closeModal={this.secondModalBtnsHandler}
          />
        )}
      </>
    );
  }
}

export default App;
