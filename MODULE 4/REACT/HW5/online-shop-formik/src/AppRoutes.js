import HomePage from "./pages/HomePage";
import FavoritesPage from "./pages/FavoritesPage";
import CartPage from "./pages/CartPage";
import { Routes, Route } from "react-router-dom";
import CheckoutFormPage from "./pages/CheckoutFormPage";

const AppRoutes = () => {
  return (
    <Routes>
      <Route path="/" element={<HomePage />} />
      <Route path="/cart" element={<CartPage />} />
      <Route path="/fav" element={<FavoritesPage />} />
      <Route path="/form" element={<CheckoutFormPage />} />
    </Routes>
  );
};

export default AppRoutes;
