import styles from "./Modal.module.scss";
import PropTypes from "prop-types";
import Button from "../Button";
import { useSelector, useDispatch } from "react-redux";
import { closeModalAC } from "../../store/modal/actionCreators";

const Modal = (props) => {
  const { text, confirmBtnHandler } = props;
  const isModalOpened = useSelector((store) => store.modal.isModalOpened);
  const modalData = useSelector((store) => store.modal.modalData);
  const dispatch = useDispatch();

  const closeModal = () => {
    dispatch(closeModalAC());
  };

  if (!isModalOpened) {
    return null;
  }

  return (
    <div className={styles.modal}>
      <div className={styles.modalBg} onClick={closeModal}></div>

      <div className={styles.modalContainer}>
        <div className={styles.modalHeader}>
          <h2 className={styles.modalTitle}>{modalData.name}</h2>
          <button className={styles.modalCloseBtn} onClick={closeModal}>
            X
          </button>
        </div>
        <div className={styles.modalBody}>{text}</div>
        <div className={styles.modalFooter}>
          <Button
            text="Confirm"
            backgroundColor="#1870CB"
            onClick={() => {
              confirmBtnHandler();
              closeModal();
            }}
          />
          <Button text="Cancel" backgroundColor="#EB0014" onClick={closeModal} />
        </div>
      </div>
    </div>
  );
};

Modal.propTypes = {
  text: PropTypes.string,
  confirmBtnHandler: PropTypes.func,
};

Modal.defaultProps = {
  text: "",
  confirmBtnHandler: () => {},
};

export default Modal;
