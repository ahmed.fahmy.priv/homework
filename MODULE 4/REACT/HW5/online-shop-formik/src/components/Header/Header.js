import styles from "./Header.module.scss";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

const Header = () => {
  const totalCartCount = useSelector((store) => store.cart.totalCartCount);
  const totalFavCount = useSelector((store) => store.cards.totalFavCount);

  return (
    <header>
      <nav>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
        </ul>
      </nav>

      <h1 className={styles.shopHeader}>Online Laptop Shop</h1>

      <ul>
        <li>
          <Link to="/cart">
            {totalCartCount === 0 ? (
              <img src="./assets/svg/cart-white-outline.svg" alt="Cart" />
            ) : (
              <img src="./assets/svg/cart-yellow-outline.svg" alt="Cart" />
            )}
            <span>{totalCartCount}</span>
          </Link>
        </li>
        <li>
          <Link to="fav">
            {totalFavCount === 0 ? (
              <img src="./assets/svg/heart-white-outline.svg" alt="Fav" />
            ) : (
              <img src="./assets/svg/heart.svg" alt="Fav" />
            )}
            <span>{totalFavCount}</span>
          </Link>
        </li>
      </ul>
    </header>
  );
};

export default Header;
