import PropTypes from "prop-types";
import styles from "./Button.module.scss";

const Button = (props) => {
  const { backgroundColor, text, onClick, type } = props;

  return (
    <button className={styles.btn} style={{ backgroundColor: backgroundColor }} onClick={onClick} type={type}>
      {text}
    </button>
  );
};

Button.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func,
  type: PropTypes.string,
};

Button.defaultProps = {
  backgroundColor: "1870CB",
  text: "Button",
  onClick: () => {},
  type: "Button",
};

export default Button;
