import { Formik, Field, Form, ErrorMessage } from "formik";
import * as yup from "yup";
import styles from "./CheckoutForm.module.scss";
import { delCartFromStorage } from "../../store/cart/actionCreators";
import { useDispatch, useSelector } from "react-redux";

const CheckoutForm = () => {
  const dispatch = useDispatch();
  const cart = useSelector((store) => store.cart.cart);

  const handleSubmit = (values, { resetForm }) => {
    dispatch(delCartFromStorage());
    resetForm();
    console.log(values);
    console.log(cart);
  };

  const initialValues = {
    name: "",
    surname: "",
    age: "",
    address: "",
    mobile: "",
  };

  const schema = yup.object().shape({
    name: yup
      .string()
      .min(3, "Your name cant be less than 3 symbols.")
      .max(24, "Your name cant be more than 24 symbols.")
      .required("Your name is required."),

    surname: yup
      .string()
      .min(2, "Your surname cant be less than 2 symbols.")
      .max(20, "Your surname cant be more than 20 symbols.")
      .required("Your surname is required."),

    age: yup
      .number()
      .min(16, "Your age cant be less than 16 years old.")
      .max(100, "Your age cant be more than 100 years old.")
      .required("Your age is required."),

    address: yup
      .string()
      .min(10, "your address cant be less than 10 symbols.")
      .max(24, "Your address cant be more than 150 symbols")
      .required("Your address is required."),

    mobile: yup
      .string()
      .matches(/\d/g, "Your mobile number can only consist of digits.")
      .required("Your mobile number is required"),
  });

  return (
    <Formik initialValues={initialValues} onSubmit={handleSubmit} validationSchema={schema}>
      {({ isValid, errors }) => {
        return (
          <Form className={styles.form}>
            <h1 className={styles.formHeader}>Checkout Form</h1>

            <Field name="name" type="text" placeholder="Name" />
            <ErrorMessage name="name">{(msg) => <span className={styles.error}>{msg}</span>}</ErrorMessage>

            <Field name="surname" type="text" placeholder="Surname" />
            <ErrorMessage name="surname">{(msg) => <span className={styles.error}>{msg}</span>}</ErrorMessage>

            <Field name="age" type="number" placeholder="Age" />
            <ErrorMessage name="age">{(msg) => <span className={styles.error}>{msg}</span>}</ErrorMessage>

            <Field name="address" type="text" placeholder="Address" />
            <ErrorMessage name="address">{(msg) => <span className={styles.error}>{msg}</span>}</ErrorMessage>

            <Field name="mobile" type="text" placeholder="Mobile number" />
            <ErrorMessage name="mobile">{(msg) => <span className={styles.error}>{msg}</span>}</ErrorMessage>

            <button className={styles.checkoutBtn} type="submit">
              Checkout
            </button>
          </Form>
        );
      }}
    </Formik>
  );
};

export default CheckoutForm;
