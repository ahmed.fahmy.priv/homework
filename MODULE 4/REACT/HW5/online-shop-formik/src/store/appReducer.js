import { combineReducers } from "redux";
import modalReducer from "./modal/modalReducer";
import cardsReducer from "./cards/cardsReducer";
import cartReducer from "./cart/cartReducer";

const appReducer = combineReducers({
  modal: modalReducer,
  cards: cardsReducer,
  cart: cartReducer,
});

export default appReducer;
