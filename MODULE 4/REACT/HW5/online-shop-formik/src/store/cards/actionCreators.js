import {
  GET_CARDS_FROM_SERVER,
  GET_CARDS_FROM_STORAGE,
  ADD_REMOVE_FROM_FAV,
  GET_FAV_COUNT_FROM_STORAGE,
  GET_FAV_ITEMS_FROM_STORAGE,
} from "./actions";

export const getCardsFromServerAC = () => async (dispatch) => {
  const cards = await fetch("./products.json").then((res) => res.json());

  dispatch({ type: GET_CARDS_FROM_SERVER, payload: cards });
};

export const getCardsFromStorageAC = (payload) => {
  return { type: GET_CARDS_FROM_STORAGE, payload };
};

export const addRemoveFromFavAC = (payload) => {
  return { type: ADD_REMOVE_FROM_FAV, payload };
};

export const getFavCountFromStorageAC = (payload) => {
  return { type: GET_FAV_COUNT_FROM_STORAGE, payload };
};

export const getFavItemsFromStorageAC = (payload) => {
  return { type: GET_FAV_ITEMS_FROM_STORAGE, payload };
};
