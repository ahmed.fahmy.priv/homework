import CheckoutForm from "../../components/CheckoutForm";

const CheckoutFormPage = () => {
  return <CheckoutForm />;
};

export default CheckoutFormPage;
