import styles from "./CartPage.module.scss";
import CartContainer from "../../components/CartContainer";
import Modal from "../../components/Modal";
import { delFromCartAC } from "../../store/cart/actionCreators";
import { useDispatch, useSelector } from "react-redux";

const CartPage = () => {
  const dispatch = useDispatch();
  const modalData = useSelector((store) => store.modal.modalData);

  return (
    <>
      <h1 className={styles.cardsHeader}>Cart</h1>

      <CartContainer />

      <Modal
        text="Are you sure you want to delete this item from the Cart?"
        confirmBtnHandler={() => dispatch(delFromCartAC(modalData))}
      />
    </>
  );
};

export default CartPage;
