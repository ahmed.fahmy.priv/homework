import Modal from "../../components/Modal/Modal";
import CardsContainer from "../../components/CardsContainer/CardsContainer";
import styles from "./FavoritesPage.module.scss";
import { useSelector, useDispatch } from "react-redux";
import { addToCartAC } from "../../store/cart/actionCreators";

const FavoritesPage = () => {
  const dispatch = useDispatch();
  const favorites = useSelector((store) => store.cards.favorites);
  const modalData = useSelector((store) => store.modal.modalData);

  return (
    <>
      <h1 className={styles.cardsHeader}>Favorites</h1>

      {favorites.length === 0 ? (
        <h1 className={styles.noItemTxt}>There are no items added to your favorites list yet.</h1>
      ) : (
        <CardsContainer cards={favorites} />
      )}

      <Modal
        text="Are you sure you want to add this item to the Cart?"
        confirmBtnHandler={() => dispatch(addToCartAC(modalData))}
      />
    </>
  );
};

export default FavoritesPage;
