import styles from "./CartItem.module.scss";
import Button from "../Button";
import PropTypes from "prop-types";
import { openModalAC, setModalDataAC } from "../../store/modal/actionCreators";
import { useDispatch } from "react-redux";

const CartItem = (props) => {
  const { name, url, code, count } = props;
  const dispatch = useDispatch();

  return (
    <div className={styles.cartItem}>
      <div className={styles.contentContainer}>
        <div className={styles.imgWrapper}>
          <img className={styles.itemAvatar} src={url} alt={name} />
        </div>
      </div>
      <span className={styles.quantity}>{name}</span>
      <span className={styles.quantity}>{count}</span>

      <div className={styles.btnContainer}>
        <Button
          color="red"
          className={styles.cartBtn}
          backgroundColor="red"
          text="DELETE"
          onClick={() => {
            dispatch(setModalDataAC({ name, code }));
            dispatch(openModalAC());
          }}
        />
      </div>
    </div>
  );
};

CartItem.propTypes = {
  name: PropTypes.string,
  url: PropTypes.string,
  code: PropTypes.number,
  count: PropTypes.number,
};

CartItem.defaultProps = {
  name: "item",
  url: "./assets/images/No-Image_Available.jpg",
  code: Math.random(),
  count: 0,
};

export default CartItem;
