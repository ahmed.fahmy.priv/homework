import CartItem from "../CartItem";
import styles from "./CartContainer.module.scss";
import { useSelector } from "react-redux";

const CartContainer = () => {
  const cart = useSelector((store) => store.cart.cart);

  if (cart.length === 0) {
    return <h1 className={styles.noItemTxt}>There are no items added to your cart yet.</h1>;
  }

  return (
    <ul>
      {cart.map(({ name, url, code, count }) => {
        return <CartItem name={name} url={url} code={code} count={count} key={code} />;
      })}
    </ul>
  );
};

export default CartContainer;
