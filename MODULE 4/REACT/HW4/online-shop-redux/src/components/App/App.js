import React, { useEffect } from "react";
import Header from "../Header/Header";
import styles from "./App.module.scss";
import AppRoutes from "../../AppRoutes";
import {
  getCardsFromServerAC,
  getCardsFromStorageAC,
  getFavCountFromStorageAC,
  getFavItemsFromStorageAC,
} from "../../store/cards/actionCreators";
import { getCartFromStorageAC, GetTotalCartCountFromStorageAC } from "../../store/cart/actionCreators";
import { useDispatch } from "react-redux";

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const cards = localStorage.getItem("cards");
    const totalCartCount = localStorage.getItem("totalCartCount");
    const totalFavCount = localStorage.getItem("totalFavCount");
    const favorites = localStorage.getItem("favorites");
    const cart = localStorage.getItem("cart");

    if (cards) {
      dispatch(getCardsFromStorageAC(JSON.parse(cards)));
    } else {
      dispatch(getCardsFromServerAC());
    }

    if (cart) {
      dispatch(getCartFromStorageAC(JSON.parse(cart)));
    }

    if (totalCartCount) {
      dispatch(GetTotalCartCountFromStorageAC(JSON.parse(totalCartCount)));
    }

    if (totalFavCount) {
      dispatch(getFavCountFromStorageAC(JSON.parse(totalFavCount)));
    }

    if (favorites) {
      dispatch(getFavItemsFromStorageAC(JSON.parse(favorites)));
    }
  }, []);

  return (
    <div className={styles.App}>
      <Header />

      <main>
        <section>
          <AppRoutes />
        </section>
      </main>
    </div>
  );
};

export default App;
