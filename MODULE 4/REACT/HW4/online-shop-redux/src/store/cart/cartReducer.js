import { ADD_TO_CART, GET_CART_FROM_STORAGE, GET_TOTAL_CART_COUNT_FROM_STORAGE, DEL_FROM_CART } from "./actions";
import produce from "immer";

const initialState = {
  cart: [],
  totalCartCount: 0,
};

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_CART: {
      return produce(state, (draftState) => {
        const newCart = draftState.cart;
        const index = newCart.findIndex((el) => el.code === action.payload.code);

        if (index === -1) {
          newCart.push({ ...action.payload, count: 1 });
        } else {
          newCart[index].count += 1;
        }

        localStorage.setItem("cart", JSON.stringify(newCart));

        draftState.totalCartCount += 1;
        localStorage.setItem("totalCartCount", draftState.totalCartCount);
      });
    }

    case DEL_FROM_CART: {
      return produce(state, (draftState) => {
        const newCart = draftState.cart;
        const index = newCart.findIndex((el) => el.code === action.payload.code);

        if (newCart[index].count > 1) {
          newCart[index].count -= 1;
        } else newCart.splice(index, 1);

        localStorage.setItem("cart", JSON.stringify(newCart));

        draftState.totalCartCount -= 1;
        localStorage.setItem("totalCartCount", draftState.totalCartCount);
      });
    }

    case GET_CART_FROM_STORAGE: {
      return produce(state, (draftState) => {
        draftState.cart = action.payload;
      });
    }

    case GET_TOTAL_CART_COUNT_FROM_STORAGE: {
      return produce(state, (draftState) => {
        draftState.totalCartCount = action.payload;
      });
    }

    default: {
      return state;
    }
  }
};

export default cartReducer;
