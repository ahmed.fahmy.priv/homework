import { PureComponent } from "react";
import styles from "./Header.module.scss";
import PropTypes from "prop-types";

class Header extends PureComponent {
  render() {
    const { favoriteCount, cartCount } = this.props;

    return (
      <header>
        <nav>
          <ul>
            <li>
              <a href="#">Home</a>
            </li>
            <li>
              <a href="#">Shop</a>
            </li>
          </ul>
        </nav>

        <h1 className={styles.shopHeader}>Online Laptop Shop</h1>

        <ul>
          <li>
            <a href="#">
              {cartCount === 0 ? (
                <img src="./assets/svg/cart-white-outline.svg" alt="Cart" />
              ) : (
                <img src="./assets/svg/cart-yellow-outline.svg" alt="Cart" />
              )}
            </a>
            <span>{cartCount}</span>
          </li>
          <li>
            <a href="#">
              {favoriteCount === 0 ? (
                <img src="./assets/svg/heart-white-outline.svg" alt="Fav" />
              ) : (
                <img src="./assets/svg/heart.svg" alt="Fav" />
              )}
            </a>
            <span>{favoriteCount}</span>
          </li>
        </ul>
      </header>
    );
  }
}

Header.propTypes = {
  favoriteCount: PropTypes.number,
  cartCount: PropTypes.number,
};

Header.defaultProps = {
  favoriteCount: 0,
  cartCount: 0,
};

export default Header;
