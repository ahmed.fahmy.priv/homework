import { PureComponent } from "react";
import styles from "./Card.module.scss";
import Button from "../Button/Button";
import PropTypes from "prop-types";

class Card extends PureComponent {
  render() {
    const { name, price, url, code, color, isFavorite, addToCartBtnHandler, addToFavorites, setModalHeader } =
      this.props;

    return (
      <div className={styles.card}>
        <button type="button" className={styles.favBttn} onClick={() => addToFavorites(code)}>
          {isFavorite ? (
            <img src="./assets/svg/heart.svg" alt="Red Heart" />
          ) : (
            <img src="./assets/svg/heart-blue-outline.svg" alt="Heart" />
          )}
        </button>
        <span className={styles.title}>{name}</span>
        <img className={styles.itemAvatar} src={url} alt="HAMMER" />

        <div className={styles.description}>
          <span className={styles.itemColor}>Color: {color}</span>
          <span className={styles.itemPrice}>{price}₴</span>
        </div>

        <div className={styles.btnContainer}>
          <Button
            backgroundColor="#1870CB"
            text="Add to Cart"
            onClick={() => {
              addToCartBtnHandler();
              setModalHeader(name);
            }}
          />
        </div>
      </div>
    );
  }
}

Card.propTypes = {
  name: PropTypes.string,
  price: PropTypes.string,
  url: PropTypes.string,
  code: PropTypes.number,
  color: PropTypes.string,
  isFavorite: PropTypes.bool,
  addToCartBtnHandler: PropTypes.func,
  addToFavorites: PropTypes.func,
};

Card.defaultProps = {
  name: "Not info Available",
  price: "0",
  url: "./assets/images/No-Image-Available.jpg",
  code: Math.random(),
  color: "Not info Available",
  isFavorite: false,
  addToCartBtnHandler: () => {},
  addToFavorites: () => {},
};

export default Card;
