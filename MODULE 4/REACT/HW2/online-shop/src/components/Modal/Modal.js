import { PureComponent } from "react";
import ModalBg from "../ModalBg/ModalBg";
import styles from "./Modal.module.scss";
import PropTypes from "prop-types";

class Modal extends PureComponent {
  render() {
    const { header, closeButton, text, actions, closeModal } = this.props;

    return (
      <div className={styles.modal}>
        <ModalBg closeModal={closeModal} />

        <div className={styles.modalContainer}>
          <div className={styles.modalHeader}>
            <h2 className={styles.modalTitle}>{header}</h2>
            {closeButton && (
              <button className={styles.modalCloseBtn} onClick={closeModal}>
                X
              </button>
            )}
          </div>
          <div className={styles.modalBody}>{text}</div>
          <div className={styles.modalFooter}>{actions}</div>
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  header: PropTypes.string,
  closeButton: PropTypes.bool,
  text: PropTypes.string,
  actions: PropTypes.element.isRequired,
  closeModal: PropTypes.func,
};

Modal.defaultProps = {
  header: "",
  closeButton: true,
  text: "",
  closeModal: () => {},
};

export default Modal;
