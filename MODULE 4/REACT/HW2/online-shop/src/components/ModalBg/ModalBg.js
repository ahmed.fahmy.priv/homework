import { PureComponent } from "react";
import styles from "./ModalBg.module.scss";
import PropTypes from "prop-types";

class ModalBg extends PureComponent {
  render() {
    const { closeModal } = this.props;

    return <div className={styles.modalBg} onClick={closeModal}></div>;
  }
}

ModalBg.propTypes = {
  closeModal: PropTypes.func,
};

ModalBg.defaultProps = {
  closeModal: () => {},
};

export default ModalBg;
