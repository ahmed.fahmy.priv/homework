import React from "react";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import Header from "../Header/Header";
import CardsContainer from "../CardsContainer/CardsContainer";
import styles from "./App.module.scss";

class App extends React.Component {
  state = {
    cards: [],
    isModalOpened: false,
    favoriteCount: 0,
    cartCount: 0,
    modalHeader: "",
  };

  async componentDidMount() {
    const cards = localStorage.getItem("cards");
    const cartCount = localStorage.getItem("cartCount");
    const favoriteCount = localStorage.getItem("favoriteCount");

    if (cards) {
      this.setState({ cards: JSON.parse(cards) });
    } else {
      const cards = await fetch("./products.json").then((res) => res.json());
      localStorage.setItem("cards", JSON.stringify(cards));
      this.setState({ cards: cards });
    }

    if (cartCount) {
      this.setState({ cartCount: JSON.parse(cartCount) });
    }

    if (favoriteCount) {
      this.setState({ favoriteCount: JSON.parse(favoriteCount) });
    }
  }

  closeModal = () => {
    this.setState({ isModalOpened: false });
  };

  addToCartBtnHandler = () => {
    this.setState({ isModalOpened: true });
  };

  addToCart = () => {
    this.setState((current) => {
      const newState = { ...current };
      newState.cartCount += 1;

      localStorage.setItem("cartCount", newState.cartCount);

      return newState;
    });

    this.closeModal();
  };

  addToFavorites = (code) => {
    this.setState((current) => {
      const newState = { ...current };
      const index = current.cards.findIndex((card) => card.code === code);

      if (!newState.cards[index].isFavorite) {
        newState.cards[index].isFavorite = true;
        localStorage.setItem("cards", JSON.stringify(newState.cards));

        newState.favoriteCount += 1;
        localStorage.setItem("favoriteCount", newState.favoriteCount);
      }

      return newState;
    });
  };

  setModalHeader = (itemTitle) => {
    this.setState({ modalHeader: itemTitle });
  };

  render() {
    const { cards, isModalOpened, favoriteCount, cartCount, modalHeader } = this.state;
    return (
      <div className={styles.App}>
        <Header favoriteCount={favoriteCount} cartCount={cartCount} />

        <main>
          <section>
            <h1 className={styles.cardsHeader}>Catalogue</h1>

            <CardsContainer
              cards={cards}
              addToCartBtnHandler={this.addToCartBtnHandler}
              addToFavorites={this.addToFavorites}
              setModalHeader={this.setModalHeader}
            />

            {isModalOpened && (
              <Modal
                header={modalHeader}
                closeButton={true}
                text="Are you sure you want to add this item to the Cart?"
                actions={
                  <>
                    <Button text="Confirm" backgroundColor="#1870CB" onClick={this.addToCart} />
                    <Button text="Cancel" backgroundColor="#EB0014" onClick={this.closeModal} />
                  </>
                }
                closeModal={this.closeModal}
              />
            )}
          </section>
        </main>
      </div>
    );
  }
}

export default App;
