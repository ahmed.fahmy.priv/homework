import React from "react";
import PropTypes from "prop-types";
import Card from "../Card/Card";
import styles from "./CardsContainer.module.scss";

class CardsContainer extends React.Component {
  render() {
    const { cards, addToCartBtnHandler, addToFavorites, setModalHeader } = this.props;

    return (
      <div>
        <ul className={styles.cardList}>
          {cards.map(({ name, price, url, code, color, isFavorite }) => (
            <li key={code}>
              <Card
                name={name}
                price={price}
                url={url}
                code={code}
                color={color}
                isFavorite={isFavorite}
                addToCartBtnHandler={addToCartBtnHandler}
                addToFavorites={addToFavorites}
                setModalHeader={setModalHeader}
              />
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

CardsContainer.propTypes = {
  cards: PropTypes.array,
  addToCartBtnHandler: PropTypes.func,
  addToFavorites: PropTypes.func,
};

CardsContainer.defaultProps = {
  cards: [],
  addToCartBtnHandler: () => {},
  addToFavorites: () => {},
};

export default CardsContainer;
