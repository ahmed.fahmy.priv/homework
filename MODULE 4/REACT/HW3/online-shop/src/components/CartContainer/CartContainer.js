import CartItem from "../CartItem";
import styles from "./CartContainer.module.scss";
import PropTypes from "prop-types";

const CartContainer = (props) => {
  const { cart, setModalProps, setIsModalOpened } = props;

  if (cart.length === 0) {
    return <h1 className={styles.cartTxt}>There are no items added in your cart yet.</h1>;
  }

  return (
    <ul>
      {cart.map(({ name, url, code, count }) => {
        return (
          <CartItem
            setModalProps={setModalProps}
            name={name}
            url={url}
            code={code}
            count={count}
            key={code}
            setIsModalOpened={setIsModalOpened}
          />
        );
      })}
    </ul>
  );
};

CartContainer.propTypes = {
  cart: PropTypes.array,
  setModalProps: PropTypes.func.isRequired,
  setIsModalOpened: PropTypes.func,
};

CartContainer.defaultProps = {
  cart: [],
  setIsModalOpened: () => false,
};

export default CartContainer;
