import styles from "./CartItem.module.scss";
import Button from "../Button";
import PropTypes from "prop-types";

const CartItem = (props) => {
  const { name, url, code, count, setModalProps, setIsModalOpened } = props;

  return (
    <div className={styles.cartItem}>
      <div className={styles.contentContainer}>
        <div className={styles.imgWrapper}>
          <img className={styles.itemAvatar} src={url} alt={name} />
        </div>
      </div>
      <span className={styles.quantity}>{name}</span>
      <span className={styles.quantity}>{count}</span>

      <div className={styles.btnContainer}>
        <Button
          color="red"
          className={styles.cartBtn}
          backgroundColor="red"
          text="DELETE"
          onClick={() => {
            setModalProps({ name, code });
            setIsModalOpened(true);
          }}
        />
      </div>
    </div>
  );
};

CartItem.propTypes = {
  name: PropTypes.string,
  url: PropTypes.string,
  code: PropTypes.number,
  count: PropTypes.number,
  setModalProps: PropTypes.func.isRequired,
  setIsModalOpened: PropTypes.func,
};

CartItem.defaultProps = {
  name: "item",
  url: "./assets/images/No-Image_Available.jpg",
  code: Math.random(),
  count: 0,
  setIsModalOpened: () => false,
};

export default CartItem;
