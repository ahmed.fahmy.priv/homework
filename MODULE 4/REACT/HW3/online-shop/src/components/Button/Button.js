import PropTypes from "prop-types";
import styles from "./Button.module.scss";

const Button = (props) => {
  const { backgroundColor, text, onClick } = props;

  return (
    <button className={styles.btn} style={{ backgroundColor: backgroundColor }} onClick={onClick}>
      {text}
    </button>
  );
};

Button.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  backgroundColor: "1870CB",
  text: "Button",
  onClick: () => {},
};

export default Button;
