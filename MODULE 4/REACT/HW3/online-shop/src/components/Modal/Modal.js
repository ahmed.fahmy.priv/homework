import styles from "./Modal.module.scss";
import PropTypes from "prop-types";
import Button from "../Button";

const Modal = (props) => {
  const { modalProps, text, confirmBtnHandler, setIsModalOpened, isModalOpened } = props;

  const closeModal = () => {
    setIsModalOpened(false);
  };

  if (!isModalOpened) {
    return null;
  }

  return (
    <div className={styles.modal}>
      <div className={styles.modalBg} onClick={closeModal}></div>

      <div className={styles.modalContainer}>
        <div className={styles.modalHeader}>
          <h2 className={styles.modalTitle}>{modalProps.name}</h2>
          <button className={styles.modalCloseBtn} onClick={closeModal}>
            X
          </button>
        </div>
        <div className={styles.modalBody}>{text}</div>
        <div className={styles.modalFooter}>
          <Button
            text="Confirm"
            backgroundColor="#1870CB"
            onClick={() => {
              confirmBtnHandler(modalProps);
              closeModal();
            }}
          />
          <Button text="Cancel" backgroundColor="#EB0014" onClick={closeModal} />
        </div>
      </div>
    </div>
  );
};

Modal.propTypes = {
  text: PropTypes.string,
  modalProps: PropTypes.object.isRequired,
  confirmBtnHandler: PropTypes.func,
  setIsModalOpened: PropTypes.func,
  isModalOpened: PropTypes.bool,
};

Modal.defaultProps = {
  text: "",
};

export default Modal;
