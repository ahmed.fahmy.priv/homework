import React from "react";
import PropTypes from "prop-types";
import Card from "../Card/Card";
import styles from "./CardsContainer.module.scss";

const CardsContainer = (props) => {
  const { cards, setIsModalOpened, addToFavorites, setModalProps } = props;

  return (
    <div>
      <ul className={styles.cardList}>
        {cards.map(({ name, price, url, code, color, isFavorite }) => (
          <li key={code}>
            <Card
              name={name}
              price={price}
              url={url}
              code={code}
              color={color}
              isFavorite={isFavorite}
              setIsModalOpened={setIsModalOpened}
              addToFavorites={addToFavorites}
              setModalProps={setModalProps}
            />
          </li>
        ))}
      </ul>
    </div>
  );
};

CardsContainer.propTypes = {
  cards: PropTypes.array,
  setIsModalOpened: PropTypes.func,
  addToFavorites: PropTypes.func,
  setModalProps: PropTypes.func.isRequired,
};

CardsContainer.defaultProps = {
  cards: [],
  setIsModalOpened: () => {},
  addToFavorites: () => {},
};

export default CardsContainer;
