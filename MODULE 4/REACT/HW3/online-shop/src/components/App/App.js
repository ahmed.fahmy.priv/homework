import React, { useState, useEffect } from "react";
import Header from "../Header/Header";
import styles from "./App.module.scss";
import AppRoutes from "../../AppRoutes";

const App = () => {
  const [cards, setCards] = useState([]);
  const [cart, setCart] = useState([]);
  const [favorites, setFavorites] = useState([]);
  const [isModalOpened, setIsModalOpened] = useState(false);
  const [favoriteCount, setFavoriteCount] = useState(0);
  const [cartCount, setCartCount] = useState(0);
  const [modalProps, setModalProps] = useState({});

  useEffect(() => {
    const cards = localStorage.getItem("cards");
    const cartCount = localStorage.getItem("cartCount");
    const favoriteCount = localStorage.getItem("favoriteCount");
    const favorites = localStorage.getItem("favorites");
    const cart = localStorage.getItem("cart");

    if (cards) {
      setCards(JSON.parse(cards));
    } else {
      (async () => {
        const cards = await fetch("./products.json").then((res) => res.json());
        localStorage.setItem("cards", JSON.stringify(cards));
        setCards(cards);
      })();
    }

    if (cartCount) {
      setCartCount(JSON.parse(cartCount));
    }

    if (favoriteCount) {
      setFavoriteCount(JSON.parse(favoriteCount));
    }

    if (favorites) {
      setFavorites(JSON.parse(favorites));
    }

    if (cart) {
      setCart(JSON.parse(cart));
    }
  }, []);

  const addToCart = (card) => {
    setCart((prev) => {
      const newCart = [...prev];
      const index = newCart.findIndex((el) => el.code === card.code);

      if (index === -1) {
        newCart.push({ ...card, count: 1 });
      } else {
        newCart[index].count += 1;
      }

      localStorage.setItem("cart", JSON.stringify(newCart));
      return newCart;
    });

    setCartCount((prev) => {
      prev += 1;
      localStorage.setItem("cartCount", prev);
      return prev;
    });
  };

  const addToFavorites = ({ name, price, url, code, color, isFavorite }) => {
    setCards((prev) => {
      const cards = [...prev];
      const index = cards.findIndex((card) => card.code === code);

      if (!cards[index].isFavorite) {
        cards[index].isFavorite = true;
        localStorage.setItem("cards", JSON.stringify(cards));

        setFavoriteCount((prev) => {
          prev += 1;
          localStorage.setItem("favoriteCount", prev);
          return prev;
        });

        setFavorites((prev) => {
          const cards = [...prev];
          cards.push({ name, price, url, code, color, isFavorite: true });
          localStorage.setItem("favorites", JSON.stringify(cards));

          return cards;
        });
      } else {
        cards[index].isFavorite = false;
        localStorage.setItem("cards", JSON.stringify(cards));

        setFavoriteCount((prev) => {
          prev -= 1;
          localStorage.setItem("favoriteCount", prev);
          return prev;
        });

        setFavorites((prev) => {
          const cards = [...prev];
          const index = cards.findIndex((card) => card.code === code);
          cards.splice(index, 1);

          localStorage.setItem("favorites", JSON.stringify(cards));

          return cards;
        });
      }

      return cards;
    });
  };

  const delFromCart = (item) => {
    setCart((prev) => {
      const newCart = [...prev];
      const index = newCart.findIndex((el) => el.code === item.code);

      if (newCart[index].count > 1) {
        newCart[index].count -= 1;
      } else newCart.splice(index, 1);

      localStorage.setItem("cart", JSON.stringify(newCart));
      return newCart;
    });

    setCartCount((prev) => {
      prev -= 1;
      localStorage.setItem("cartCount", prev);
      return prev;
    });
  };

  return (
    <div className={styles.App}>
      <Header favoriteCount={favoriteCount} cartCount={cartCount} />

      <main>
        <section>
          <AppRoutes
            cards={cards}
            setIsModalOpened={setIsModalOpened}
            addToFavorites={addToFavorites}
            isModalOpened={isModalOpened}
            modalProps={modalProps}
            addToCart={addToCart}
            favorites={favorites}
            setModalProps={setModalProps}
            cart={cart}
            delFromCart={delFromCart}
          />
        </section>
      </main>
    </div>
  );
};

export default App;
