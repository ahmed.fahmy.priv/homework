import styles from "./Header.module.scss";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const Header = (props) => {
  const { favoriteCount, cartCount } = props;

  return (
    <header>
      <nav>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
        </ul>
      </nav>

      <h1 className={styles.shopHeader}>Online Laptop Shop</h1>

      <ul>
        <li>
          <Link to="/cart">
            {cartCount === 0 ? (
              <img src="./assets/svg/cart-white-outline.svg" alt="Cart" />
            ) : (
              <img src="./assets/svg/cart-yellow-outline.svg" alt="Cart" />
            )}
            <span>{cartCount}</span>
          </Link>
        </li>
        <li>
          <Link to="fav">
            {favoriteCount === 0 ? (
              <img src="./assets/svg/heart-white-outline.svg" alt="Fav" />
            ) : (
              <img src="./assets/svg/heart.svg" alt="Fav" />
            )}
            <span>{favoriteCount}</span>
          </Link>
        </li>
      </ul>
    </header>
  );
};

Header.propTypes = {
  favoriteCount: PropTypes.number,
  cartCount: PropTypes.number,
};

Header.defaultProps = {
  favoriteCount: 0,
  cartCount: 0,
};

export default Header;
