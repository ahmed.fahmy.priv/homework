import HomePage from "./pages/HomePage";
import FavoritesPage from "./pages/FavoritesPage";
import CartPage from "./pages/CartPage";
import { Routes, Route } from "react-router-dom";

const AppRoutes = (props) => {
  const {
    cards,
    isModalOpened,
    modalProps,
    setIsModalOpened,
    addToFavorites,
    addToCart,
    favorites,
    setModalProps,
    cart,
    delFromCart,
  } = props;

  return (
    <Routes>
      <Route
        path="/"
        element={
          <HomePage
            cards={cards}
            setIsModalOpened={setIsModalOpened}
            addToFavorites={addToFavorites}
            isModalOpened={isModalOpened}
            modalProps={modalProps}
            addToCart={addToCart}
            setModalProps={setModalProps}
          />
        }
      />
      <Route
        path="/cart"
        element={
          <CartPage
            cart={cart}
            setModalProps={setModalProps}
            setIsModalOpened={setIsModalOpened}
            modalProps={modalProps}
            isModalOpened={isModalOpened}
            delFromCart={delFromCart}
          />
        }
      />
      <Route
        path="/fav"
        element={
          <FavoritesPage
            favorites={favorites}
            setIsModalOpened={setIsModalOpened}
            addToFavorites={addToFavorites}
            isModalOpened={isModalOpened}
            modalProps={modalProps}
            addToCart={addToCart}
            setModalProps={setModalProps}
          />
        }
      />
    </Routes>
  );
};

export default AppRoutes;
