import Modal from "../../components/Modal";
import CardsContainer from "../../components/CardsContainer/CardsContainer";
import styles from "./HomePage.module.scss";

const HomePage = (props) => {
  const { cards, setIsModalOpened, addToFavorites, isModalOpened, modalProps, addToCart, setModalProps } = props;

  return (
    <>
      <h1 className={styles.cardsHeader}>Catalogue</h1>

      <CardsContainer
        cards={cards}
        setIsModalOpened={setIsModalOpened}
        addToFavorites={addToFavorites}
        setModalProps={setModalProps}
      />

      <Modal
        modalProps={modalProps}
        text="Are you sure you want to add this item to the Cart?"
        confirmBtnHandler={addToCart}
        setIsModalOpened={setIsModalOpened}
        isModalOpened={isModalOpened}
      />
    </>
  );
};

export default HomePage;
