import Modal from "../../components/Modal/Modal";
import CardsContainer from "../../components/CardsContainer/CardsContainer";
import styles from "./FavoritesPage.module.scss";

const FavoritesPage = (props) => {
  const { favorites, setIsModalOpened, addToFavorites, isModalOpened, modalProps, addToCart, setModalProps } = props;

  return (
    <>
      <h1 className={styles.cardsHeader}>Favorites</h1>

      <CardsContainer
        cards={favorites}
        setIsModalOpened={setIsModalOpened}
        addToFavorites={addToFavorites}
        setModalProps={setModalProps}
      />

      <Modal
        modalProps={modalProps}
        text="Are you sure you want to add this item to the Cart?"
        confirmBtnHandler={addToCart}
        setIsModalOpened={setIsModalOpened}
        isModalOpened={isModalOpened}
      />
    </>
  );
};

export default FavoritesPage;
