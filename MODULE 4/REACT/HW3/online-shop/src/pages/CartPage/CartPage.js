import styles from "./CartPage.module.scss";
import CartContainer from "../../components/CartContainer";
import Modal from "../../components/Modal";

const CartPage = (props) => {
  const { cart, setModalProps, setIsModalOpened, modalProps, isModalOpened, delFromCart } = props;

  return (
    <>
      <h1 className={styles.cardsHeader}>Cart</h1>
      <CartContainer cart={cart} setModalProps={setModalProps} setIsModalOpened={setIsModalOpened} />
      <Modal
        modalProps={modalProps}
        text="Are you sure you want to delete this item from the Cart?"
        confirmBtnHandler={delFromCart}
        setIsModalOpened={setIsModalOpened}
        isModalOpened={isModalOpened}
      />
    </>
  );
};

export default CartPage;
