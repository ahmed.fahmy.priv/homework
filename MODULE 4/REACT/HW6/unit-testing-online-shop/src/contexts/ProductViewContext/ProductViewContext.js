import { createContext } from "react";

const ProductViewContext = createContext();

export default ProductViewContext;
