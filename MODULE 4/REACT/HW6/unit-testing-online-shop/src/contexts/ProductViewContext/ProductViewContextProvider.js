import { useState } from "react";
import ProductViewContext from "./ProductViewContext";

const ProductViewContextProvider = ({ children }) => {
  const [isCardsView, setIsCardsView] = useState(true);

  return <ProductViewContext.Provider value={{ isCardsView, setIsCardsView }}>{children}</ProductViewContext.Provider>;
};
export default ProductViewContextProvider;
