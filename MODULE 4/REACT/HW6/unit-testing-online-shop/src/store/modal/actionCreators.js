import { OPEN_MODAL, CLOSE_MODAL, SET_MODAL_DATA } from "./actions";

export const openModalAC = () => {
  return { type: OPEN_MODAL };
};

export const closeModalAC = () => {
  return { type: CLOSE_MODAL };
};

export const setModalDataAC = (payload) => {
  return { type: SET_MODAL_DATA, payload };
};
