import { OPEN_MODAL, CLOSE_MODAL, SET_MODAL_DATA } from "./actions";
import produce from "immer";

const initialState = {
  isModalOpened: false,
  modalData: {},
};

const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case OPEN_MODAL: {
      return produce(state, (draftState) => {
        draftState.isModalOpened = true;
      });
    }

    case CLOSE_MODAL: {
      return produce(state, (draftState) => {
        draftState.isModalOpened = false;
      });
    }

    case SET_MODAL_DATA: {
      return produce(state, (draftState) => {
        draftState.modalData = action.payload;
      });
    }

    default: {
      return state;
    }
  }
};

export default modalReducer;
