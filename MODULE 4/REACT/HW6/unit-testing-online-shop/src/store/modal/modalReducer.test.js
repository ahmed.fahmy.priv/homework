import modalReducer from "./modalReducer";
import { OPEN_MODAL, CLOSE_MODAL, SET_MODAL_DATA } from "./actions";

const initialState = {
  isModalOpened: false,
  modalData: {},
};

describe("modalReducer works", () => {
  test("should return initial state if no arguments given", () => {
    expect(modalReducer(undefined, { type: undefined })).toEqual(initialState);
  });

  test("should change isModalOpened to true", () => {
    expect(modalReducer(initialState, { type: OPEN_MODAL })).toEqual({ isModalOpened: true, modalData: {} });
  });

  test("should change isModalOpened to false", () => {
    expect(modalReducer(initialState, { type: CLOSE_MODAL })).toEqual({ isModalOpened: false, modalData: {} });
  });

  test("should set modalData", () => {
    expect(modalReducer(initialState, { type: SET_MODAL_DATA, payload: { name: "test" } })).toEqual({
      isModalOpened: false,
      modalData: { name: "test" },
    });
  });
});
