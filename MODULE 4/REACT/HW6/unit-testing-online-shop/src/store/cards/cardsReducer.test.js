import cardsReducer from "./cardsReducer";
import {
  GET_CARDS_FROM_SERVER,
  GET_CARDS_FROM_STORAGE,
  ADD_REMOVE_FROM_FAV,
  GET_FAV_COUNT_FROM_STORAGE,
  GET_FAV_ITEMS_FROM_STORAGE,
} from "./actions";

const initialState = {
  cards: [],
  favorites: [],
  totalFavCount: 0,
};

describe("cards reducer works", () => {
  test("should get cards from server and save them in state", () => {
    expect(cardsReducer(initialState, { type: GET_CARDS_FROM_SERVER, payload: ["test"] })).toEqual({
      cards: ["test"],
      favorites: [],
      totalFavCount: 0,
    });
  });

  test("should get cards from storage and save them in state", () => {
    expect(cardsReducer(initialState, { type: GET_CARDS_FROM_STORAGE, payload: ["test"] })).toEqual({
      cards: ["test"],
      favorites: [],
      totalFavCount: 0,
    });
  });

  test("add card to favorites", () => {
    expect(
      cardsReducer(
        {
          cards: [
            {
              name: "test name",
              price: "test price",
              url: "test url",
              code: "test code",
              color: "test color",
              isFavorite: false,
            },
          ],
          favorites: [],
          totalFavCount: 0,
        },
        {
          type: ADD_REMOVE_FROM_FAV,
          payload: { name: "test name", price: "test price", url: "test url", code: "test code", color: "test color" },
        }
      )
    ).toEqual({
      cards: [
        {
          name: "test name",
          price: "test price",
          url: "test url",
          code: "test code",
          color: "test color",
          isFavorite: true,
        },
      ],
      favorites: [
        {
          name: "test name",
          price: "test price",
          url: "test url",
          code: "test code",
          color: "test color",
          isFavorite: true,
        },
      ],
      totalFavCount: 1,
    });
  });

  test("remove card to favorites", () => {
    expect(
      cardsReducer(
        {
          cards: [
            {
              name: "test name",
              price: "test price",
              url: "test url",
              code: "test code",
              color: "test color",
              isFavorite: true,
            },
          ],
          favorites: [],
          totalFavCount: 1,
        },
        {
          type: ADD_REMOVE_FROM_FAV,
          payload: { name: "test name", price: "test price", url: "test url", code: "test code", color: "test color" },
        }
      )
    ).toEqual({
      cards: [
        {
          name: "test name",
          price: "test price",
          url: "test url",
          code: "test code",
          color: "test color",
          isFavorite: false,
        },
      ],
      favorites: [],
      totalFavCount: 0,
    });
  });

  test("should get totalFavCount from storage and save them in state", () => {
    expect(cardsReducer(initialState, { type: GET_FAV_COUNT_FROM_STORAGE, payload: "test" })).toEqual({
      cards: [],
      favorites: [],
      totalFavCount: "test",
    });
  });

  test("should get favorite items from storage and save them in state", () => {
    expect(cardsReducer(initialState, { type: GET_FAV_ITEMS_FROM_STORAGE, payload: ["test"] })).toEqual({
      cards: [],
      favorites: ["test"],
      totalFavCount: 0,
    });
  });
});
