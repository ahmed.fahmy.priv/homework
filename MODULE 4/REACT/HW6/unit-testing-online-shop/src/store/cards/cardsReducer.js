import {
  GET_CARDS_FROM_SERVER,
  GET_CARDS_FROM_STORAGE,
  ADD_REMOVE_FROM_FAV,
  GET_FAV_COUNT_FROM_STORAGE,
  GET_FAV_ITEMS_FROM_STORAGE,
} from "./actions";
import produce from "immer";

const initialState = {
  cards: [],
  favorites: [],
  totalFavCount: 0,
};

const cardsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_CARDS_FROM_SERVER: {
      return produce(state, (draftState) => {
        draftState.cards = action.payload;

        localStorage.setItem("cards", JSON.stringify(draftState.cards));
      });
    }

    case GET_CARDS_FROM_STORAGE: {
      return produce(state, (draftState) => {
        draftState.cards = action.payload;
      });
    }

    case ADD_REMOVE_FROM_FAV: {
      const { name, price, url, code, color } = action.payload;

      return produce(state, (draftState) => {
        const newCards = draftState.cards;
        const newFavorites = draftState.favorites;
        const cardIndex = newCards.findIndex((card) => card.code === action.payload.code);

        if (!newCards[cardIndex].isFavorite) {
          newCards[cardIndex].isFavorite = true;
          localStorage.setItem("cards", JSON.stringify(newCards));

          draftState.totalFavCount += 1;
          localStorage.setItem("totalFavCount", draftState.totalFavCount);

          newFavorites.push({ name, price, url, code, color, isFavorite: true });
          localStorage.setItem("favorites", JSON.stringify(newFavorites));
        } else {
          const favIndex = newFavorites.findIndex((card) => card.code === action.payload.code);

          newCards[cardIndex].isFavorite = false;
          localStorage.setItem("cards", JSON.stringify(newCards));

          draftState.totalFavCount -= 1;
          localStorage.setItem("totalFavCount", draftState.totalFavCount);

          newFavorites.splice(favIndex, 1);
          localStorage.setItem("favorites", JSON.stringify(newFavorites));
        }
      });
    }

    case GET_FAV_ITEMS_FROM_STORAGE: {
      return produce(state, (draftState) => {
        draftState.favorites = action.payload;
      });
    }

    case GET_FAV_COUNT_FROM_STORAGE: {
      return produce(state, (draftState) => {
        draftState.totalFavCount = action.payload;
      });
    }

    default: {
      return state;
    }
  }
};

export default cardsReducer;
