import {
  ADD_TO_CART,
  GET_CART_FROM_STORAGE,
  GET_TOTAL_CART_COUNT_FROM_STORAGE,
  DEL_FROM_CART,
  DEL_CART_FROM_STORAGE,
} from "./actions";

export const addToCartAC = (payload) => {
  return { type: ADD_TO_CART, payload };
};

export const getCartFromStorageAC = (payload) => {
  return { type: GET_CART_FROM_STORAGE, payload };
};

export const GetTotalCartCountFromStorageAC = (payload) => {
  return { type: GET_TOTAL_CART_COUNT_FROM_STORAGE, payload };
};

export const delFromCartAC = (payload) => {
  return { type: DEL_FROM_CART, payload };
};

export const delCartFromStorage = () => {
  return { type: DEL_CART_FROM_STORAGE };
};
