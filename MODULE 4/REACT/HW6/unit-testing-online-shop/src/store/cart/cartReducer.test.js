import cartReducer from "./cartReducer";
import {
  ADD_TO_CART,
  GET_CART_FROM_STORAGE,
  GET_TOTAL_CART_COUNT_FROM_STORAGE,
  DEL_FROM_CART,
  DEL_CART_FROM_STORAGE,
} from "./actions";

const initialState = {
  cart: [],
  totalCartCount: 0,
};

describe("cart reducer works", () => {
  test("should return initial state if no arguments given to reducer", () => {
    expect(cartReducer(undefined, { type: undefined })).toEqual(initialState);
  });

  test("should add to cart new item", () => {
    expect(cartReducer(initialState, { type: ADD_TO_CART, payload: { name: "test name 1" } })).toEqual({
      cart: [{ name: "test name 1", count: 1 }],
      totalCartCount: 1,
    });
  });

  test("should add to cart existing item", () => {
    expect(
      cartReducer(
        { cart: [{ name: "test name 1", count: 1 }], totalCartCount: 1 },
        { type: ADD_TO_CART, payload: { test } }
      )
    ).toEqual({
      cart: [{ name: "test name 1", count: 2 }],
      totalCartCount: 2,
    });
  });

  test("should get cart from storage", () => {
    expect(cartReducer(initialState, { type: GET_CART_FROM_STORAGE, payload: ["test"] })).toEqual({
      cart: ["test"],
      totalCartCount: 0,
    });
  });

  test("should get totalCartCount from storage", () => {
    expect(cartReducer(initialState, { type: GET_TOTAL_CART_COUNT_FROM_STORAGE, payload: "test" })).toEqual({
      cart: [],
      totalCartCount: "test",
    });
  });

  test("should delete from cart", () => {
    expect(
      cartReducer(
        { cart: [{ name: "test name 1", count: 1 }], totalCartCount: 1 },
        { type: DEL_FROM_CART, payload: { test } }
      )
    ).toEqual({
      cart: [],
      totalCartCount: 0,
    });
  });

  test("should delete cart from storage", () => {
    expect(
      cartReducer(
        {
          cart: [
            { name: "test-name 1", count: 1 },
            { name: "test name 2", count: 2 },
          ],
          totalCartCount: 2,
        },
        { type: DEL_CART_FROM_STORAGE }
      )
    ).toEqual({
      cart: [],
      totalCartCount: 0,
    });
  });
});
