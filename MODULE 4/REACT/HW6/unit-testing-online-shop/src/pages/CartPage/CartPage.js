import styles from "./CartPage.module.scss";
import CartContainer from "../../components/CartContainer";
import Modal from "../../components/Modal";
import Button from "../../components/Button";
import { delFromCartAC } from "../../store/cart/actionCreators";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

const CartPage = () => {
  const dispatch = useDispatch();
  const modalData = useSelector((store) => store.modal.modalData);

  const navigate = useNavigate();

  return (
    <>
      <h1 className={styles.cardsHeader}>Cart</h1>

      <CartContainer />

      <Button
        text="Proceed to checkout"
        backgroundColor="#1870cb"
        onClick={() => {
          navigate("/form");
        }}
      ></Button>

      <Modal
        text="Are you sure you want to delete this item from the Cart?"
        confirmBtnHandler={() => dispatch(delFromCartAC(modalData))}
      />
    </>
  );
};

export default CartPage;
