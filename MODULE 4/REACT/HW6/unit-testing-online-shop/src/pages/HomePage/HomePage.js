import Modal from "../../components/Modal";
import CardsContainer from "../../components/CardsContainer/CardsContainer";
import styles from "./HomePage.module.scss";
import { useDispatch, useSelector } from "react-redux";
import { addToCartAC } from "../../store/cart/actionCreators";
import { Button } from "@mui/material";
import { useContext } from "react";
import ProductViewContext from "../../contexts/ProductViewContext/ProductViewContext";

const HomePage = () => {
  const { setIsCardsView } = useContext(ProductViewContext);
  const dispatch = useDispatch();
  const cards = useSelector((store) => store.cards.cards);
  const modalData = useSelector((store) => store.modal.modalData);

  return (
    <>
      <div className={styles.viewBtnwrapper}>
        <Button variant="contained" onClick={() => setIsCardsView(true)}>
          Cards
        </Button>
        <Button variant="outlined" onClick={() => setIsCardsView(false)}>
          Table
        </Button>
      </div>
      <h1 className={styles.cardsHeader}>Catalogue</h1>
      <CardsContainer cards={cards} />
      <Modal
        text="Are you sure you want to add this item to the Cart?"
        confirmBtnHandler={() => dispatch(addToCartAC(modalData))}
      />
    </>
  );
};

export default HomePage;
