import { render } from "@testing-library/react";
import Card from "../Card/Card";
import { Provider } from "react-redux";
import store from "../../store";

const MockedProvider = ({ name, price, url, color, isFavorite }) => {
  return (
    <Provider store={store}>
      <Card name="name" price="price" url="url" color="color" isFavorite={true} />
    </Provider>
  );
};

describe("Card snapshot test", () => {
  test("should Card render with no erros", () => {
    const { asFragment } = render(
      <MockedProvider name="name" price="price" url="url" color="color" isFavorite={true} />,
      { store }
    );

    expect(asFragment()).toMatchSnapshot();
  });
});
