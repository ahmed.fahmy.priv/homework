import Button from "./Button";
import { render, fireEvent, screen } from "@testing-library/react";

const buttonClickHandler = jest.fn();

describe("Button snapshot test", () => {
  test("should Button render with no erros", () => {
    const { asFragment } = render(<Button text="test text" type="Button" backgroundColor="white" />);

    expect(asFragment()).toMatchSnapshot();
  });
});

describe("Button click works", () => {
  test("should onClick function be called when button clicked", () => {
    render(<Button text="test text" type="Button" backgroundColor="white" onClick={buttonClickHandler} />);

    fireEvent.click(screen.getByText("test text"));

    expect(buttonClickHandler).toHaveBeenCalled();
  });
});
