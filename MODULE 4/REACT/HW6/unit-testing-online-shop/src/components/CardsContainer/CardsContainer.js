import React, { useContext } from "react";
import PropTypes from "prop-types";
import Card from "../Card/Card";
import styles from "./CardsContainer.module.scss";
import ProductViewContext from "../../contexts/ProductViewContext/ProductViewContext";
import ItemTable from "../ItemTable";

const CardsContainer = (props) => {
  const { cards } = props;
  const { isCardsView } = useContext(ProductViewContext);

  if (!isCardsView) {
    return <ItemTable cards={cards} />;
  }

  return (
    <div>
      <ul className={styles.cardList}>
        {cards.map(({ name, price, url, code, color, isFavorite }) => (
          <li key={code}>
            <Card name={name} price={price} url={url} code={code} color={color} isFavorite={isFavorite} />
          </li>
        ))}
      </ul>
    </div>
  );
};

CardsContainer.propTypes = {
  cards: PropTypes.array,
};

CardsContainer.defaultProps = {
  cards: [],
};

export default CardsContainer;
