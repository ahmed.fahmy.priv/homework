import Header from "./Header";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import store from "../../store";
import { BrowserRouter } from "react-router-dom";

const MockedProvider = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Header />
      </BrowserRouter>
    </Provider>
  );
};

describe("Header snapshot test", () => {
  test("should Header render with no errors", () => {
    const { asFragment } = render(<MockedProvider />, { store });
    expect(asFragment()).toMatchSnapshot();
  });
});
