import { render } from "@testing-library/react";
import CartItem from "../CartItem";
import { Provider } from "react-redux";
import store from "../../store";

const MockedProvider = ({ name, url, count }) => {
  return (
    <Provider store={store}>
      <CartItem name="name" url="url" count={0} />
    </Provider>
  );
};

describe("CartItem snapshot test", () => {
  test("should CartItem render with no erros", () => {
    const { asFragment } = render(<MockedProvider name="name" url="url" count="count" />, { store });

    expect(asFragment()).toMatchSnapshot();
  });
});
