import Modal from "./Modal";
import { render, fireEvent, screen } from "@testing-library/react";
import { Provider, useDispatch } from "react-redux";
import store from "../../store";
import { openModalAC, closeModalAC } from "../../store/modal/actionCreators";

const confirmBtnHandler = jest.fn();

const MockedWrapper = ({ text, confirmBtnHandler }) => {
  const dispatch = useDispatch();

  return (
    <>
      <Modal text={text} confirmBtnHandler={confirmBtnHandler} />
      <button
        onClick={() => {
          dispatch(openModalAC());
        }}
      >
        OPEN
      </button>
      <button
        onClick={() => {
          dispatch(closeModalAC());
        }}
      >
        CLOSE
      </button>
    </>
  );
};

const MockedProvider = ({ text, confirmBtnHandler }) => {
  return (
    <Provider store={store}>
      <MockedWrapper text={text} confirmBtnHandler={confirmBtnHandler} />
    </Provider>
  );
};

describe("Modal snapshot test", () => {
  test("should Modal render with no errors when isModalOpened is true", () => {
    const { asFragment } = render(<MockedProvider text="test text" />, { store });

    fireEvent.click(screen.getByText("OPEN"));

    expect(asFragment()).toMatchSnapshot();
  });
});

describe("Modal opens and closes on state changes", () => {
  test("should Modal open when isModalOpened is true", () => {
    render(<MockedProvider text="test text" />, { store });

    fireEvent.click(screen.getByText("OPEN"));

    expect(screen.getByTestId("modal-root")).toBeInTheDocument();
  });

  test("should Modal close when isModalOpened is false", () => {
    render(<MockedProvider text="test text" />, { store });

    fireEvent.click(screen.getByText("OPEN"));
    fireEvent.click(screen.getByText("CLOSE"));

    expect(screen.queryByTestId("modal-root")).not.toBeInTheDocument();
  });
});

describe("Modal confirmBtnHandler works", () => {
  test("should confirmBtnHandler be called when confirm btn is clicked", () => {
    render(<MockedProvider text="test text" confirmBtnHandler={confirmBtnHandler} />, { store });

    fireEvent.click(screen.getByText("OPEN"));
    fireEvent.click(screen.getByText("Confirm"));

    expect(confirmBtnHandler).toHaveBeenCalled();
  });
});

describe("Modal closeModal fn works", () => {
  test("should closeModal be called when bg is clicked", () => {
    render(<MockedProvider text="test text" />, { store });

    fireEvent.click(screen.getByText("OPEN"));
    fireEvent.click(screen.getByTestId("modal-bg"));

    expect(screen.queryByTestId("modal-root")).not.toBeInTheDocument();
  });

  test("should closeModal be called when X is clicked", () => {
    render(<MockedProvider text="test text" />, { store });

    fireEvent.click(screen.getByText("OPEN"));
    fireEvent.click(screen.getByText("X"));

    expect(screen.queryByTestId("modal-root")).not.toBeInTheDocument();
  });

  test("should closeModal be called when Cancel is clicked", () => {
    render(<MockedProvider text="test text" />, { store });

    fireEvent.click(screen.getByText("OPEN"));
    fireEvent.click(screen.getByText("Cancel"));

    expect(screen.queryByTestId("modal-root")).not.toBeInTheDocument();
  });
});
