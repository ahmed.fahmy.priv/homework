const ipifyUrl = "https://api.ipify.org?format=json";
const locateBttn = document.querySelector("button");

class Card {
  constructor(country, regionName, city, ip) {
    this.country = country;
    this.regionName = regionName;
    this.city = city;
    this.ip = ip;
  }

  render(container) {
    container.innerHTML = `<div class="card">
    <h2 class="ip">${this.ip}</h2>
    <p class="text">is located at</p>
    <p class="country">Country: ${this.country}</p>
    <p class="region">Region: ${this.regionName}</p>
    <p class="city">City: ${this.city}</p>
  </div>`;
  }
}

const getIp = async () => {
  try {
    const { ip } = await fetch(ipifyUrl).then((response) => response.json());

    return ip;
  } catch (err) {
    console.warn(`Something went wrong while fetching your ip address. ${err.message}`);
  }
};

const getLocation = async () => {
  try {
    const ip = await getIp();
    const locationUrl = `http://ip-api.com/json/${ip}?fields=status,country,regionName,city,query`;

    const location = await fetch(locationUrl).then((response) => response.json());

    return location;
  } catch (err) {
    console.warn(`Something went wrong while finding your location. ${err.message}`);
  }
};

const showLocation = async () => {
  const content = document.querySelector(".content");
  try {
    const { status, country, regionName, city, query } = await getLocation();

    if (status === "success") {
      new Card(country, regionName, city, query).render(content);
    }
  } catch (err) {
    content.innerHTML = "<h1>Unable to determine your location at the moment, please try again later.</h1>";
  }
};

locateBttn.addEventListener("click", showLocation);
