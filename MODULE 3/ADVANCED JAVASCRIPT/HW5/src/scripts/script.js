class Card {
  constructor(fullName, email, title, text, postId, deleteBttnHandler, editBttnHandler) {
    this.fullName = fullName;
    this.email = email;
    this.cardHeader = document.createElement("h3");
    this.cardText = document.createElement("p");
    this.title = title;
    this.text = text;
    this.postId = postId;
    this.deleteBttnHandler = deleteBttnHandler;
    this.editBttnHandler = editBttnHandler;
    this.card = document.createElement("div");
    this.deleteBttn = document.createElement("button");
    this.editBttn = document.createElement("button");
  }

  createElements() {
    this.card.classList.add("card");
    this.deleteBttn.classList.add("delete-bttn");
    this.editBttn.classList.add("edit-bttn");
    this.cardHeader.classList.add("title");
    this.cardText.classList.add("text");
    this.cardHeader.innerHTML = this.title;
    this.cardText.innerHTML = this.text;

    this.card.insertAdjacentHTML(
      "beforeend",
      `<div class="flex-wrapper">
        <div class="avatar"></div>
        <div class="header-wrapper">
          <h2 class="name">${this.fullName}</h2>
          <span class="email">${this.email}</span>
        </div>
      </div>`
    );

    this.card.append(this.cardHeader, this.cardText, this.editBttn, this.deleteBttn);

    this.deleteBttn.addEventListener("click", this.deleteBttnHandler.bind(this));
    this.editBttn.addEventListener("click", this.editBttnHandler.bind(this));
  }
}

class loadedCard extends Card {
  render(container) {
    this.createElements();
    container.append(this.card);
  }
}

class addedCard extends Card {
  render(container) {
    this.createElements();
    container.prepend(this.card);
  }
}

class Modal {
  constructor() {
    this.modal = document.createElement("div");
    this.modalBg = document.createElement("div");
    this.bttnWrapper = document.createElement("div");
    this.cancelBttn = document.createElement("button");
  }

  closeModal() {
    this.modal.remove();
  }

  createElements() {
    this.cancelBttn.classList.add("modal-cancel-bttn");
    this.cancelBttn.innerText = "CANCEL";
    this.cancelBttn.type = "button";

    this.modalBg.classList.add("modal__background");
    this.modal.classList.add("modal");
    this.bttnWrapper.classList.add("modal__button-wrapper");

    this.bttnWrapper.append(this.cancelBttn);
    this.modal.append(this.modalBg);

    this.cancelBttn.addEventListener("click", () => {
      this.closeModal();
    });
    this.modalBg.addEventListener("click", () => {
      this.closeModal();
    });
  }

  render(container) {
    this.createElements();
    container.prepend(this.modal);
  }
}

class addModal extends Modal {
  constructor(addPostFn) {
    super();
    this.addPostFn = addPostFn;
    this.addConfirmBttn = document.createElement("button");
    this.addForm = document.createElement("form");
    this.addInput = document.createElement("input");
    this.addTextArea = document.createElement("textarea");
  }

  createElements() {
    super.createElements();

    this.addForm.classList.add("form");
    this.addInput.placeholder = "Please enter your title.";
    this.addTextArea.placeholder = "Please enter your text.";

    this.addConfirmBttn.classList.add("modal-confirm-bttn");
    this.addConfirmBttn.innerText = "CONFIRM";

    this.bttnWrapper.prepend(this.addConfirmBttn);
    this.addForm.append(this.addInput, this.addTextArea, this.bttnWrapper);
    this.modal.append(this.addForm);

    this.addConfirmBttn.addEventListener("click", this.addPostFn.bind(this));
  }
}

class deleteModal extends Modal {
  constructor(cardTitle, deletePostFn) {
    super();
    this.cardTitle = cardTitle;
    this.deletePostFn = deletePostFn;
    this.deleteConfirmBttn = document.createElement("button");
    this.modalContentContainer = document.createElement("div");
  }

  createElements() {
    super.createElements();
    this.deleteConfirmBttn.classList.add("modal-confirm-bttn");
    this.deleteConfirmBttn.innerText = "CONFIRM";
    this.modalContentContainer.classList.add("modal-content");

    this.bttnWrapper.prepend(this.deleteConfirmBttn);

    this.modalContentContainer.insertAdjacentHTML(
      "beforeend",
      `<h3>Do you really want to delete "${this.cardTitle}"?</h3>`
    );

    this.modalContentContainer.append(this.bttnWrapper);

    this.modal.append(this.modalContentContainer);

    this.deleteConfirmBttn.addEventListener("click", () => {
      this.deletePostFn();
      this.closeModal();
    });
  }
}

class editModal extends Modal {
  constructor(cardTitle, cardText, editPostFn) {
    super();
    this.cardTitle = cardTitle;
    this.cardText = cardText;
    this.editPostFn = editPostFn;
    this.editForm = document.createElement("form");
    this.editInput = document.createElement("input");
    this.editTextArea = document.createElement("textarea");
    this.editConfirmBttn = document.createElement("button");
  }

  createElements() {
    super.createElements();

    this.editForm.classList.add("form");
    this.editConfirmBttn.classList.add("modal-confirm-bttn");
    this.editConfirmBttn.innerText = "CONFIRM";
    this.editInput.value = this.cardTitle;
    this.editTextArea.value = this.cardText;

    this.bttnWrapper.prepend(this.editConfirmBttn);
    this.editForm.append(this.editInput, this.editTextArea, this.bttnWrapper);
    this.modal.append(this.editForm);

    this.editConfirmBttn.addEventListener("click", (e) => {
      e.preventDefault();
      this.editPostFn(this.editInput.value, this.editTextArea.value);
      this.closeModal();
    });
  }
}

const getUsers = () => {
  return fetch("https://ajax.test-danit.com/api/json/users")
    .then((response) => response.json())
    .catch((err) => console.warn(`${err}: Something went wrong while loading user cards`));
};

const getPosts = () => {
  return fetch("https://ajax.test-danit.com/api/json/posts").then((response) => response.json());
};

const showUsersPosts = () => {
  const placeholders = document.querySelectorAll(".placeholder");
  getUsers().then((usersData) => {
    usersData.forEach(({ name, email, id }) => {
      getPosts()
        .then((postsData) => {
          placeholders.forEach((item) => item.remove());
          postsData.forEach(({ userId, title, body, id: postId }) => {
            if (userId === id) {
              new loadedCard(name, email, title, body, postId, deleteBttnHandler, editBttnHandler).render(
                document.querySelector(".card-container")
              );
            }
          });
        })
        .catch((err) => console.warn(err, "Something went wrong while loading user posts"));
    });
  });
};

function deleteBttnHandler() {
  const deletePostFn = () => {
    fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
      method: "DELETE",
    })
      .then(({ status }) => {
        if (status === 200) {
          this.card.remove();
        } else {
          throw new Error(`Status ${status}: Something went wrong while deleting your post`);
        }
      })
      .catch((err) => alert(err.message));
  };
  new deleteModal(this.title, deletePostFn).render(document.querySelector(".container"));
}

function editBttnHandler() {
  const editPostFn = (editedTitle, editedText) => {
    fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
      method: "PUT",
      body: JSON.stringify({
        title: editedTitle,
        body: editedText,
      }),
    })
      .then(({ status }) => {
        if (status === 200) {
          this.cardHeader.innerHTML = editedTitle;
          this.cardText.innerHTML = editedText;
          this.title = editedTitle;
          this.text = editedText;
        } else {
          throw new Error(`Status ${status}: Something went wrong while editing your post`);
        }
      })
      .catch((err) => alert(err.message));
  };
  new editModal(this.title, this.text, editPostFn).render(document.querySelector(".container"));
}

function addPostFn(e) {
  e.preventDefault();
  fetch("https://ajax.test-danit.com/api/json/posts", {
    method: "POST",
    body: JSON.stringify({
      userId: 1,
      title: this.addInput.value,
      body: this.addTextArea.value,
    }),
  })
    .then((response) => response.json())
    .then(({ title, body, id: postId }) => {
      fetch("https://ajax.test-danit.com/api/json/users/1")
        .then((response) => response.json())
        .then(({ name, email }) => {
          new addedCard(name, email, title, body, postId, deleteBttnHandler, editBttnHandler).render(
            document.querySelector(".card-container")
          );
        });
    })
    .catch((err) => console.warn(`${err}, Something went wrong while adding your post.`));

  this.closeModal();
}

const addPostBttn = document.querySelector(".add-post-bttn").addEventListener("click", () => {
  new addModal(addPostFn).render(document.querySelector(".container"));
});

showUsersPosts();
