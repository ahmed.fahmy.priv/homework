// Задание:

// - Дан массив books.

// - Выведите этот массив на экран в виде списка (тег ul - список должен быть сгенерирован с помощью Javascript).

// - На странице должен находиться div с id="root", куда и нужно будет положить этот список (похожая задача была дана в модуле basic).

// - Перед выводом обьекта на странице, нужно проверить его на корректность (в объекте должны содержаться все три свойства - author, name, price). Если какого-то из этих свойств нету, в консоли должна высветиться ошибка с указанием - какого свойства нету в обьекте.

// - Те элементы массива, которые являются некорректными по условиям предыдущего пункта, не должны появиться на странице.

const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

const divContainer = document.querySelector("#root");

class PropertyMissing extends Error {
  constructor(value) {
    super();
    this.name = "Book Property Missing";
    this.message = `The ${value} property of this book is missing.`;
  }
}

class BookObject {
  constructor(book) {
    if (!book.author) {
      throw new PropertyMissing("Author");
    } else if (!book.name) {
      throw new PropertyMissing("Name");
    } else if (!book.price) {
      throw new PropertyMissing("Price");
    }
    this.bookAuthor = book.author;
    this.bookName = book.name;
    this.bookPrice = book.price;
  }

  render(container) {
    container.insertAdjacentHTML(
      "beforeend",
      `<ul>
        <li>Author: ${this.bookAuthor}</li>
        <li>Name: ${this.bookName}</li>
        <li>Price: ${this.bookPrice}</li>
      </ul>`
    );
  }
}

books.forEach((item) => {
  try {
    new BookObject(item).render(divContainer);
  } catch (err) {
    if (err instanceof PropertyMissing) {
      console.warn(err);
    } else throw err;
  }
});
