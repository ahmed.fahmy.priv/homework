### Теоретический вопрос

##### **Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript**

- Прототипное наследование это самое главное способности языка ЙС. у каждого объекта в ЖС есть свойсто прототип (**proto**) которое держит ссылки на другои объект "PROTOTYPE" который держит в себе все свойства и методы доступны этому объекту.

- Прототипное наследование это образ от которого каждый объект в ЖС способен наследовать свойства и методы другово объекта.

- Работает это образом который называется прототипоной цепочкой. Когда мы зовем свойства или метод в объекте, то объект ищет их в себе, если не найдет то ищет в своем прототипе, если не найдет то ищет в прототипе своего прототипа (если сущетвует) и так продолжается пока найдется свойства или пока дойдет до базовово объекта JS который является прототип всех прототипов.
