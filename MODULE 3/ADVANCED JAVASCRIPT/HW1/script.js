// - Реализовать класс Employee, в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата). Сделайте так, чтобы эти свойства заполнялись при создании объекта.             DONE

// - Создайте геттеры и сеттеры для этих свойств.        DONE

// - Создайте класс Programmer, который будет наследоваться от класса Employee, и у которого будет свойство lang (список языков).  DONE

// - Для класса Programmer перезапишите геттер для свойства salary. Пусть он возвращает свойство salary, умноженное на 3.   DONE

// - Создайте несколько экземпляров обьекта Programmer, выведите их в консоль.   DONE

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary + "$";
  }
  get name() {
    return this._name;
  }

  get age() {
    return this._age;
  }

  get salary() {
    return this._salary;
  }

  set name(value) {
    this._name = value;
  }

  set age(value) {
    this._age = value;
  }

  set salary(value) {
    this._salary = value;
  }
}

class Programmer extends Employee {
  constructor(language, ...args) {
    super(...args);
    this._lang = language;
  }

  get salary() {
    return parseInt(this._salary) * 3 + "$";
  }
}

const employee1 = new Programmer("JavaScript", "Ahmed", 39, 5000);
const employee2 = new Programmer("Java", "Alan", 35, 4000);
const employee3 = new Programmer("Python", "Darya", 30, 3000);

console.log(employee1);
console.log(employee2);
console.log(employee3);
