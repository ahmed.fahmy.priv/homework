////////////////////CARD CLASS CREATED WHICH WILL BE USED TO PLACE EACH MOVIE AS A CARD IN THE DOM CONTAINER/////////////////
class Card {
  constructor(filmName, episode, crawl) {
    this.filmName = filmName;
    this.episode = episode;
    this.crawl = crawl;
    this.card = document.createElement("div");
  }

  createElements() {
    this.card.classList.add("card");

    this.card.insertAdjacentHTML(
      "beforeend",
      `<h1 class="name">${this.filmName}</h1>
       <h2 class="episode">Episode: ${this.episode}</h2>
       <p class="text">${this.crawl}</p>
       <h3>Characters:</h3>
       <ul class="list"><div class="loader"></div></ul>`
    );
  }

  render(container = document.body) {
    this.createElements();
    container.append(this.card);
  }
}

///////////////URL TO BE FETCHED SAVED IN A CONSTANT/////////////////////////////////////////////
const filmsUrl = "https://ajax.test-danit.com/api/swapi/films";

//////////////////FIRST SOLUTION PERFORMING PARALLEL FETCH REQUESTS BY PROMISE.ALLSETTLED METHOD////////////////
fetch(filmsUrl)
  .then((response) => response.json())

  .then((filmsArray) => {
    filmsArray.forEach(({ name: filmName, episodeId, openingCrawl, characters }, index) => {
      new Card(filmName, episodeId, openingCrawl).render(document.querySelector(".container"));

      const characterPromiseArr = characters.map((url) => fetch(url).then((response) => response.json()));
      Promise.allSettled(characterPromiseArr).then((charData) => {
        charData.forEach((character) => {
          if (character.status === "fulfilled") {
            const {
              value: { name },
            } = character;

            if (document.querySelectorAll(".loader")[index]) {
              document.querySelectorAll(".loader")[index].remove();
            }

            document
              .querySelectorAll(".list")
              [index].insertAdjacentHTML("beforeend", `<li class="list__item">${name}</li>`);
          } else throw new Error("Character error:");
        });
      });
    });
  })
  .catch((err) => {
    document.querySelector(".container").innerHTML = `<h1>Something went wrong: ${err.status}: ${err.message}</h1>`;
  });

//////////////////ANOTHER SOLUTION USING FOR EACH CYCLE TO PERFORM SYNCHRONOUS FETCH REQUESTS////////////////

// fetch(filmsUrl)
//   .then((response) => response.json())

//   .then((filmsArray) => {
//     filmsArray.forEach(({ name, episodeId, openingCrawl, characters }, index) => {
//       new Card(name, episodeId, openingCrawl).render(document.querySelector(".container"));

//       characters.forEach((url) =>
//         fetch(url)
//           .then((response) => response.json())
//           .then(({ name }) => {
//             if (document.querySelectorAll(".loader")[index]) {
//               document.querySelectorAll(".loader")[index].remove();
//             }
//             document
//               .querySelectorAll(".list")
//               [index].insertAdjacentHTML("beforeend", `<li class="list__item">${name}</li>`);
//           })
//       );
//     });
//   });
